package cl.jfcor.abastible_fb.BaseDatos;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import cl.jfcor.abastible_fb.Config.Constantes;
import cl.jfcor.abastible_fb.Interfaces.OrdenLecturaDAO;
import cl.jfcor.abastible_fb.Interfaces.PerfilDAO;
import cl.jfcor.abastible_fb.Interfaces.RutaDAO;
import cl.jfcor.abastible_fb.Interfaces.UsuarioDAO;
import cl.jfcor.abastible_fb.Entidades.OrdenLectura;
import cl.jfcor.abastible_fb.Entidades.Perfil;
import cl.jfcor.abastible_fb.Entidades.Ruta;
import cl.jfcor.abastible_fb.Entidades.Usuario;


@Database(entities = {
        Perfil.class,
        Usuario.class,
        OrdenLectura.class,
        Ruta.class}, version = 1, exportSchema = false)
public abstract class AbastibleDB extends RoomDatabase {

    private static AbastibleDB INSTANCE;

    public abstract PerfilDAO perfilsDAO();

    public abstract UsuarioDAO usuariosDAO();

    public abstract OrdenLecturaDAO ordenLecturaDAO();

    public abstract RutaDAO rutaDAO();

    public static AbastibleDB getAppDb(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AbastibleDB.class,
                    Constantes.NAME_DATABASE)
                    .addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build();

            //                    .allowMainThreadQueries()
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(Constantes.TABLE_PERFILS);
            database.execSQL(Constantes.TABLE_USUARIOS);
        }
    };


    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
