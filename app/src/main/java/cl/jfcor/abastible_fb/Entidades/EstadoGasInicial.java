package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_ESTADO_GAS_INICIAL)
public class EstadoGasInicial {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "nombre")
    private String nombre;

    public EstadoGasInicial() {
    }

    public EstadoGasInicial(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
