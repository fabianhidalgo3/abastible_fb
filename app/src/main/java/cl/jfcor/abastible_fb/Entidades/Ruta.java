package cl.jfcor.abastible_fb.Entidades;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_RUTA)
public class Ruta {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "codigo")
    private String codigo;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "mes")
    private int mes;

    @ColumnInfo(name = "ano")
    private int ano;

    @ColumnInfo(name = "abierto")
    private int abierto;

    @ColumnInfo(name = "porcion_id")
    private int porcion_id;


    public Ruta(int id, String codigo, String nombre, int mes, int ano, int abierto, int porcion_id) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.mes = mes;
        this.ano = ano;
        this.abierto = abierto;
        this.porcion_id = porcion_id;
    }

    public Ruta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getAbierto() {
        return abierto;
    }

    public void setAbierto(int abierto) {
        this.abierto = abierto;
    }

    public int getPorcion_id() {
        return porcion_id;
    }

    public void setPorcion_id(int porcion_id) {
        this.porcion_id = porcion_id;
    }
}
