package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_CLIENTES)
public class Cliente {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "numero_cliente")
    private int numero_cliente;

    @ColumnInfo(name = "calle")
    private String calle;

    @ColumnInfo(name = "numero_domicilio")
    private String numero_domicilio;

    @ColumnInfo(name = "direccion_completa")
    private String direccion_completa;

    public Cliente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero_cliente() {
        return numero_cliente;
    }

    public void setNumero_cliente(int numero_cliente) {
        this.numero_cliente = numero_cliente;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero_domicilio() {
        return numero_domicilio;
    }

    public void setNumero_domicilio(String numero_domicilio) {
        this.numero_domicilio = numero_domicilio;
    }

    public String getDireccion_completa() {
        return direccion_completa;
    }

    public void setDireccion_completa(String direccion_completa) {
        this.direccion_completa = direccion_completa;
    }
}
