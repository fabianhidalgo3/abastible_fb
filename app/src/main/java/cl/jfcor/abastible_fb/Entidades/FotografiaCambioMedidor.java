package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

import cl.jfcor.abastible_fb.Config.Constantes;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = Constantes.NAME_TABLE_FOTOGRAFIA_CAMBIO_MEDIDOR,
        foreignKeys = @ForeignKey(entity = OrdenCambioMedidor.class,
                parentColumns = "id",
                childColumns = "orden_cambio_medidor_id",
                onDelete = CASCADE))
public class FotografiaCambioMedidor {
}
