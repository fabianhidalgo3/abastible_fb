package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_USUARIOS/*,
        foreignKeys = @ForeignKey(entity = Perfil.class,
        parentColumns = "id",
        childColumns = "perfil_id",
        onDelete = CASCADE)*/)
public class Usuario {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "encrypted_password")
    private String encrypted_password;

    @ColumnInfo(name = "reset_password_token")
    private String reset_password_token;

    @ColumnInfo(name = "reset_password_sent_at")
    private String reset_password_sent_at;

    @ColumnInfo(name = "remember_created_at")
    private String remember_created_at;

    @ColumnInfo(name = "sign_in_count")
    private int sign_in_count;

    @ColumnInfo(name = "current_sign_in_at")
    private String current_sign_in_at;

    @ColumnInfo(name = "last_sign_in_at")
    private String last_sign_in_at;

    @ColumnInfo(name = "current_sign_in_ip")
    private String current_sign_in_ip;

    @ColumnInfo(name = "last_sign_in_ip")
    private String last_sign_in_ip;

    @ColumnInfo(name = "created_at")
    private String created_at;

    @ColumnInfo(name = "updated_at")
    private String updated_at;

    @ColumnInfo(name = "perfil_id")
    private int perfil_id;

    @ColumnInfo(name = "authentication_token")
    private String authentication_token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncrypted_password() {
        return encrypted_password;
    }

    public void setEncrypted_password(String encrypted_password) {
        this.encrypted_password = encrypted_password;
    }

    public String getReset_password_token() {
        return reset_password_token;
    }

    public void setReset_password_token(String reset_password_token) {
        this.reset_password_token = reset_password_token;
    }

    public String getReset_password_sent_at() {
        return reset_password_sent_at;
    }

    public void setReset_password_sent_at(String reset_password_sent_at) {
        this.reset_password_sent_at = reset_password_sent_at;
    }

    public String getRemember_created_at() {
        return remember_created_at;
    }

    public void setRemember_created_at(String remember_created_at) {
        this.remember_created_at = remember_created_at;
    }

    public int getSign_in_count() {
        return sign_in_count;
    }

    public void setSign_in_count(int sign_in_count) {
        this.sign_in_count = sign_in_count;
    }

    public String getCurrent_sign_in_at() {
        return current_sign_in_at;
    }

    public void setCurrent_sign_in_at(String current_sign_in_at) {
        this.current_sign_in_at = current_sign_in_at;
    }

    public String getLast_sign_in_at() {
        return last_sign_in_at;
    }

    public void setLast_sign_in_at(String last_sign_in_at) {
        this.last_sign_in_at = last_sign_in_at;
    }

    public String getCurrent_sign_in_ip() {
        return current_sign_in_ip;
    }

    public void setCurrent_sign_in_ip(String current_sign_in_ip) {
        this.current_sign_in_ip = current_sign_in_ip;
    }

    public String getLast_sign_in_ip() {
        return last_sign_in_ip;
    }

    public void setLast_sign_in_ip(String last_sign_in_ip) {
        this.last_sign_in_ip = last_sign_in_ip;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getPerfil_id() {
        return perfil_id;
    }

    public void setPerfil_id(int perfil_id) {
        this.perfil_id = perfil_id;
    }

    public String getAuthentication_token() {
        return authentication_token;
    }

    public void setAuthentication_token(String authentication_token) {
        this.authentication_token = authentication_token;
    }

    @Ignore
    public Usuario(String email, String encrypted_password) {
        this.email = email;
        this.encrypted_password = encrypted_password;
    }

    public Usuario() {

    }

}
