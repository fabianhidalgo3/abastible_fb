package cl.jfcor.abastible_fb.Entidades;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = Constantes.NAME_TABLE_ORDEN_LECTURAS,
        foreignKeys = {
                @ForeignKey(entity = Instalacion.class,
                        parentColumns = "id",
                        childColumns = "instalacion_id", onDelete = CASCADE),
                @ForeignKey(entity = TipoLectura.class,
                        parentColumns = "id",
                        childColumns = "tipo_lectura_id", onDelete = CASCADE),
                @ForeignKey(entity = EstadoLectura.class,
                        parentColumns = "id",
                        childColumns = "estado_lectura_id", onDelete = CASCADE),
                //@ForeignKey(entity = Comuna.class,
                  //      parentColumns = "id",
                    //    childColumns = "comuna_id", onDelete = CASCADE),
                @ForeignKey(entity = Ruta.class,
                        parentColumns = "id",
                        childColumns = "ruta_id", onDelete = CASCADE),
                @ForeignKey(entity = Medidor.class,
                        parentColumns = "id",
                        childColumns = "medidor_id", onDelete = CASCADE),
                @ForeignKey(entity = Cliente.class,
                        parentColumns = "id",
                        childColumns = "cliente_id", onDelete = CASCADE)
        })
public class OrdenLectura {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "codigo")
    private String codigo;

    @ColumnInfo(name = "secuenciar_lector")
    private int secuencia_lector;

    @ColumnInfo(name = "direccion")
    private String direccion;

    @ColumnInfo(name = "observacion_lector")
    private String observacion_lector;

    @ColumnInfo(name = "gps_latitud")
    private String gps_latitud;

    @ColumnInfo(name = "gps_longitud")
    private String gps_longitud;

    @ColumnInfo(name = "medidor_id")
    private int medidor_id;

    @ColumnInfo(name = "instalacion_id")
    private int instalacion_id;

    @ColumnInfo(name = "cliente_id")
    private int cliente_id;

    @ColumnInfo(name = "ruta_id")
    private int ruta_id;

    @ColumnInfo(name = "tipo_lectura_id")
    private int tipo_lectura_id;

    @ColumnInfo(name = "estado_lectura_id")
    private int estado_lectura_id;

    @ColumnInfo(name = "color")
    private int color;

    @ColumnInfo(name = "communa_id")
    private int communa_id;

    public OrdenLectura() {
    }

    public OrdenLectura(int id, String codigo, int secuencia_lector, String direccion, String direccion_entrega, String numero_poste, String observacion_lector, String fecha_carga, String fecha_propuesta, String fecha_asignacion, String gps_latitud, String gps_longitud, String ajuste_sencillo_anterior, String ajuste_sencillo_actual, int verificacion, int modulo_analisis, int aprobado, String observacion_analista, int color, int medidor_id, int instalacion_id, int cliente_id) {
        this.id = id;
        this.codigo = codigo;
        this.secuencia_lector = secuencia_lector;
        this.direccion = direccion;
        this.observacion_lector = observacion_lector;
        this.gps_latitud = gps_latitud;
        this.gps_longitud = gps_longitud;
        this.color = color;
        this.medidor_id = medidor_id;
        this.instalacion_id = instalacion_id;
        this.cliente_id = cliente_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getSecuencia_lector() {
        return secuencia_lector;
    }

    public void setSecuencia_lector(int secuencia_lector) {
        this.secuencia_lector = secuencia_lector;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getObservacion_lector() {
        return observacion_lector;
    }

    public void setObservacion_lector(String observacion_lector) {
        this.observacion_lector = observacion_lector;
    }

    public String getGps_latitud() {
        return gps_latitud;
    }

    public void setGps_latitud(String gps_latitud) {
        this.gps_latitud = gps_latitud;
    }

    public String getGps_longitud() {
        return gps_longitud;
    }

    public void setGps_longitud(String gps_longitud) {
        this.gps_longitud = gps_longitud;
    }

    public int getMedidor_id() {
        return medidor_id;
    }

    public void setMedidor_id(int medidor_id) {
        this.medidor_id = medidor_id;
    }

    public int getInstalacion_id() {
        return instalacion_id;
    }

    public void setInstalacion_id(int instalacion_id) {
        this.instalacion_id = instalacion_id;
    }

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }

    public int getRuta_id() {
        return ruta_id;
    }

    public void setRuta_id(int ruta_id) {
        this.ruta_id = ruta_id;
    }

    public int getTipo_lectura_id() {
        return tipo_lectura_id;
    }

    public void setTipo_lectura_id(int tipo_lectura_id) {
        this.tipo_lectura_id = tipo_lectura_id;
    }

    public int getEstado_lectura_id() {
        return estado_lectura_id;
    }

    public void setEstado_lectura_id(int estado_lectura_id) {
        this.estado_lectura_id = estado_lectura_id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getCommuna_id() {
        return communa_id;
    }

    public void setCommuna_id(int communa_id) {
        this.communa_id = communa_id;
    }
}
