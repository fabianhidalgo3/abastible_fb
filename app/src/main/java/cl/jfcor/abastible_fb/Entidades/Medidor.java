package cl.jfcor.abastible_fb.Entidades;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_MEDIDOR)
public class Medidor {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "numero_medidor")
    private String numero_medidor;

    @ColumnInfo(name = "propiedad_cliente")
    private int propiedad_cliente;

    @ColumnInfo(name = "numero_digitos")
    private int numero_digitos;

    @ColumnInfo(name = "diametro")
    private int diametro;

    @ColumnInfo(name = "ano")
    private int ano;

    @ColumnInfo(name = "ubicacion_medidor")
    private String ubicacion_medidor;

    @ColumnInfo(name = "created_at")
    private String created_at;

    @ColumnInfo(name = "updated_at")
    private String updated_at;

    @ColumnInfo(name = "modelo_medidor_id")
    private int modelo_medidor_id;

    public Medidor(int id, String numero_medidor, int propiedad_cliente, int numero_digitos, int diametro, int ano, String ubicacion_medidor, int modelo_medidor_id) {
        this.id = id;
        this.numero_medidor = numero_medidor;
        this.propiedad_cliente = propiedad_cliente;
        this.numero_digitos = numero_digitos;
        this.diametro = diametro;
        this.ano = ano;
        this.ubicacion_medidor = ubicacion_medidor;
        this.modelo_medidor_id = modelo_medidor_id;
    }

    public Medidor() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero_medidor() {
        return numero_medidor;
    }

    public void setNumero_medidor(String numero_medidor) {
        this.numero_medidor = numero_medidor;
    }

    public int getPropiedad_cliente() {
        return propiedad_cliente;
    }

    public void setPropiedad_cliente(int propiedad_cliente) {
        this.propiedad_cliente = propiedad_cliente;
    }

    public int getNumero_digitos() {
        return numero_digitos;
    }

    public void setNumero_digitos(int numero_digitos) {
        this.numero_digitos = numero_digitos;
    }

    public int getDiametro() {
        return diametro;
    }

    public void setDiametro(int diametro) {
        this.diametro = diametro;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getUbicacion_medidor() {
        return ubicacion_medidor;
    }

    public void setUbicacion_medidor(String ubicacion_medidor) {
        this.ubicacion_medidor = ubicacion_medidor;
    }
    public int getModelo_medidor_id() {
        return modelo_medidor_id;
    }

    public void setModelo_medidor_id(int modelo_medidor_id) {
        this.modelo_medidor_id = modelo_medidor_id;
    }
}
