package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_CLAVE_CONVENIO)

public class ClaveConvenio {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private  int id;

    @ColumnInfo(name = "codigo")
    private String codigo;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "requerido")
    private int requerido;

    @ColumnInfo(name = "efectivo")
    private int efectivo;

    @ColumnInfo(name = "numero_fotografias")
    private int numero_fotografias;

    @ColumnInfo(name = "activo")
    private int activo;

    @ColumnInfo(name = "secuencia")
    private  int secuencia;

    public ClaveConvenio() {
    }

    public ClaveConvenio(int id, String codigo, String nombre, int requerido, int efectivo, int numero_fotografias, int activo, int secuencia) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.requerido = requerido;
        this.efectivo = efectivo;
        this.numero_fotografias = numero_fotografias;
        this.activo = activo;
        this.secuencia = secuencia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getRequerido() {
        return requerido;
    }

    public void setRequerido(int requerido) {
        this.requerido = requerido;
    }

    public int getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(int efectivo) {
        this.efectivo = efectivo;
    }

    public int getNumero_fotografias() {
        return numero_fotografias;
    }

    public void setNumero_fotografias(int numero_fotografias) {
        this.numero_fotografias = numero_fotografias;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
}
