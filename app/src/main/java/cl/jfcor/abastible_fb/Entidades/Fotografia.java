package cl.jfcor.abastible_fb.Entidades;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import static android.arch.persistence.room.ForeignKey.CASCADE;
import cl.jfcor.abastible_fb.Config.Constantes;

@Entity(tableName = Constantes.NAME_TABLE_FOTOGRAFIA,
        foreignKeys = @ForeignKey(entity = OrdenLectura.class,
                parentColumns = "id",
                childColumns = "orden_lectura_id",
                onDelete = CASCADE))
public class Fotografia {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "detalle_orden_lectura_id")
    private int detalle_orden_lectura_id;

    @ColumnInfo(name = "archivo")
    private String archivo;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    public Fotografia() {
    }

    public Fotografia(int id, int detalle_orden_lectura_id, String archivo, String descripcion) {
        this.id = id;
        this.detalle_orden_lectura_id = detalle_orden_lectura_id;
        this.archivo = archivo;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDetalle_orden_lectura_id() {
        return detalle_orden_lectura_id;
    }

    public void setDetalle_orden_lectura_id(int detalle_orden_lectura_id) {
        this.detalle_orden_lectura_id = detalle_orden_lectura_id;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
