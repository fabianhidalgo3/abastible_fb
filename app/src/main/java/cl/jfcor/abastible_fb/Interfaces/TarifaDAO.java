package cl.jfcor.abastible_fb.Interfaces;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import cl.jfcor.abastible_fb.modelos.Tarifa;


@Dao
public interface TarifaDAO {
    @Insert
    void insert(Tarifa tarifa);

    @Query("SELECT * FROM tarifas ")
    Tarifa find();

    @Update
    void update(Tarifa tarifa);

    @Query("DELETE FROM tarifas")
    void deleteAll();

    @Delete
    void delete(Tarifa tarifa);
}
