package cl.jfcor.abastible_fb.Interfaces;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.Comuna;

@Dao
public interface ComunaDAO {
    @Insert
    void insert(Comuna comuna);

    @Query("SELECT * FROM comunas ")
    Comuna find();

    @Update
    void update(Comuna comuna);

    @Query("DELETE FROM comunas")
    void deleteAll();

    @Delete
    void delete(Comuna comuna);
}
