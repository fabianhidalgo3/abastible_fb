package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import cl.jfcor.abastible_fb.modelos.Ruta;

@Dao
public interface RutaDAO {
    @Insert
    void insert(Ruta ruta);

    @Query("SELECT * FROM ruta ")
    Ruta find();

    @Update
    void update(Ruta ruta);

    @Query("DELETE FROM ruta")
    void deleteAll();

    @Delete
    void delete(Ruta ruta);
}
