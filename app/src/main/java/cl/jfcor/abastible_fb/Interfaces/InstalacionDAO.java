package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.Instalacion;


@Dao
public interface InstalacionDAO {
    @Insert
    void insert(Instalacion instalacion);

    @Query("SELECT * FROM instalacion ")
    Instalacion find();

    @Update
    void update(Instalacion instalacion);

    @Query("DELETE FROM instalacion")
    void deleteAll();

    @Delete
    void delete(Instalacion instalacion);
}
