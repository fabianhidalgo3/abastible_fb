package cl.jfcor.abastible_fb.Interfaces;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.Cliente;

@Dao
public interface ClienteDAO {
    @Insert
    void insert(Cliente cliente);

    @Query("SELECT * FROM clientes ")
    Cliente find();

    @Update
    void update(Cliente clientes);

    @Query("DELETE FROM clientes")
    void deleteAll();

    @Delete
    void delete(Cliente clientes);
}
