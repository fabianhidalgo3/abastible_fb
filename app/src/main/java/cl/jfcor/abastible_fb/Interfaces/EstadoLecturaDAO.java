package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.EstadoLectura;

@Dao
public interface EstadoLecturaDAO {
    @Insert
    void insert(EstadoLectura estadoLecturas);

    @Query("SELECT * FROM estado_lecturas ")
    EstadoLectura find();

    @Update
    void update(EstadoLectura estadoLecturas);

    @Query("DELETE FROM estado_lecturas")
    void deleteAll();

    @Delete
    void delete(EstadoLectura estadoLecturas);
}
