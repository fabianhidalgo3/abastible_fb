package cl.jfcor.abastible_fb.Interfaces;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.TipoLectura;

@Dao
public interface TipoLecturaDAO {
    @Insert
    void insert(TipoLectura tipoLectura);

    @Query("SELECT * FROM tipo_lecturas ")
    TipoLectura find();

    @Update
    void update(TipoLectura tipoLectura);

    @Query("DELETE FROM tipo_lecturas")
    void deleteAll();

    @Delete
    void delete(TipoLectura tipoLectura);
}
