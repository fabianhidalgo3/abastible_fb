package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cl.jfcor.abastible_fb.modelos.OrdenLectura;

@Dao
public interface OrdenLecturaDAO {
    @Insert
    void insert(OrdenLectura ordenLectura);

    @Query("SELECT * FROM orden_lecturas")
    List<OrdenLectura> getAllOrdenLecturas();

    @Update
    void update(OrdenLectura ordenLectura);

    @Query("DELETE FROM orden_lecturas")
    void deleteAll();

    @Delete
    void delete(OrdenLectura ordenLectura);
}
