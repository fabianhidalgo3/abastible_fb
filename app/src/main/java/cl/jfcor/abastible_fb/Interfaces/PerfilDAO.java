package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cl.jfcor.abastible_fb.Entidades.Perfil;

@Dao
public interface PerfilDAO {

    @Insert
    void insert(Perfil perfil);

    @Insert()
    void insertPerfils(List<Perfil> list);

    @Query("SELECT * FROM perfiles ")
    Perfil find();

    @Update
    void update(Perfil perfil);

    @Query("DELETE FROM perfiles")
    void deleteAll();

    @Delete
    void delete(Perfil perfil);
}
