package cl.jfcor.abastible_fb.Interfaces;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import cl.jfcor.abastible_fb.Entidades.Fotografia;

@Dao
public interface FotografiaDAO {
    @Insert
    void insert(Fotografia fotografia);

    @Query("SELECT * FROM fotografia ")
    Fotografia find();

    @Update
    void update(Fotografia fotografia);

    @Query("DELETE FROM fotografia")
    void deleteAll();

    @Delete
    void delete(Fotografia fotografia);
}
