package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import cl.jfcor.abastible_fb.Entidades.ClaveLectura;

@Dao
public interface ClaveLecturaDAO {

    @Insert
    void insert(ClaveLectura claveLecturas);

    @Query("SELECT * FROM clave_lecturas")
    ClaveLectura find();

    @Update
    void updateAsignacionLecturas(ClaveLectura claveLecturas);

    @Query("DELETE FROM clave_lecturas")
    void deleteAll();

    @Delete
    void delete(ClaveLectura claveLecturas);
}
