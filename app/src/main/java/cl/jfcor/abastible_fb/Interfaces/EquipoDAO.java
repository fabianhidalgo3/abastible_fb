package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import cl.jfcor.abastible_fb.Entidades.Equipo;

@Dao
public interface EquipoDAO {
    @Insert
    void insert(Equipo equipo);

    @Query("SELECT * FROM equipos")
    Equipo find();

    @Update
    void updateUsuarioByEmail(Equipo equipo);

    @Query("DELETE FROM equipos")
    void deleteAll();

    @Delete
    void delete(Equipo equipo);
}
