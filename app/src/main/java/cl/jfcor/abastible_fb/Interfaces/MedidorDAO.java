package cl.jfcor.abastible_fb.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import cl.jfcor.abastible_fb.modelos.Medidor;

@Dao
public interface MedidorDAO {
    @Insert
    void insert(Medidor medidor);

    @Query("SELECT * FROM medidor ")
    Medidor find();

    @Update
    void update(Medidor medidor);

    @Query("DELETE FROM medidor")
    void deleteAll();

    @Delete
    void delete(Medidor medidor);
}
