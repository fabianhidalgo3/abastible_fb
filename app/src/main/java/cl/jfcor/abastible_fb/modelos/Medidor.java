package cl.jfcor.abastible_fb.modelos;

public class Medidor {
    private int id;
    private String numeroMedidor;
    private String numeroDigitos;
    private int diametro;
    private int ano;
    private String ubicacionMedidor;
    private int modeloMedidorId;

    public Medidor(int id, String numeroMedidor, String numeroDigitos, String ubicacionMedidor, int modeloMedidorId) {
        this.id = id;
        this.numeroMedidor = numeroMedidor;
        this.numeroDigitos = numeroDigitos;
        this.ubicacionMedidor = ubicacionMedidor;
        this.modeloMedidorId = modeloMedidorId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    public String getNumeroDigitos() {
        return numeroDigitos;
    }

    public void setNumeroDigitos(String numeroDigitos) {
        this.numeroDigitos = numeroDigitos;
    }

    public int getDiametro() {
        return diametro;
    }

    public void setDiametro(int diametro) {
        this.diametro = diametro;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getUbicacionMedidor() {
        return ubicacionMedidor;
    }

    public void setUbicacionMedidor(String ubicacionMedidor) {
        this.ubicacionMedidor = ubicacionMedidor;
    }

    public int getModeloMedidorId() {
        return modeloMedidorId;
    }

    public void setModeloMedidorId(int modeloMedidorId) {
        this.modeloMedidorId = modeloMedidorId;
    }
}
