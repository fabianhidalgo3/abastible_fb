package cl.jfcor.abastible_fb.modelos;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
// Creado por Fabián Hidalgo

public class OrdenReparto implements Serializable{

    // Declaro los atritutos de la clase
    private int id;
    private String codigo;
    private String direccion;
    private int estadoRepartoId;
    private int secuenciaLector;
    private int instalacionId;
    private int medidorId;
    private int rutaId;
    private int tipoDocumentoId;
    private int clienteId;
    private double gpsLatitud;
    private double gpsLongitud;
    private long fechaEjecucion;
    private int flagEnvio;
    private String numeroCliente;
    // Creo constructor Vacio
    public OrdenReparto() { }

    // Creo un constructor con todos los datos
    public OrdenReparto(int id, String codigo, String direccion, int estadoRepartoId, int secuenciaLector, int instalacionId, int medidorId, int rutaId, int tipoDocumentoId, int clienteId, double gpsLatitud, double gpsLongitud, long fechaEjecucion, int flagEnvio) {
        this.id = id;
        this.codigo = codigo;
        this.direccion = direccion;
        this.estadoRepartoId = estadoRepartoId;
        this.secuenciaLector = secuenciaLector;
        this.instalacionId = instalacionId;
        this.medidorId = medidorId;
        this.rutaId = rutaId;
        this.tipoDocumentoId = tipoDocumentoId;
        this.clienteId = clienteId;
        this.gpsLatitud = gpsLatitud;
        this.gpsLongitud = gpsLongitud;
        this.fechaEjecucion = fechaEjecucion;
        this.flagEnvio = flagEnvio;
    }

    public OrdenReparto(int id, String codigo, String direccion, int estadoRepartoId, int secuenciaLector, int instalacionId, int medidorId, int rutaId, int tipoDocumentoId, int clienteId, String numeroCliente) {
        this.id = id;
        this.codigo = codigo;
        this.direccion = direccion;
        this.estadoRepartoId = estadoRepartoId;
        this.secuenciaLector = secuenciaLector;
        this.instalacionId = instalacionId;
        this.medidorId = medidorId;
        this.rutaId = rutaId;
        this.tipoDocumentoId = tipoDocumentoId;
        this.clienteId = clienteId;
        this.numeroCliente = numeroCliente;
    }

    public String parseFechaEjecucion(){
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = dateFormatter.format(this.fechaEjecucion);
        return fecha;
    }

    // Creo Get and Set

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getEstadoRepartoId() {
        return estadoRepartoId;
    }

    public void setEstadoRepartoId(int estadoRepartoId) {
        this.estadoRepartoId = estadoRepartoId;
    }

    public long getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(long fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public int getSecuenciaLector() {
        return secuenciaLector;
    }

    public void setSecuenciaLector(int secuenciaLector) {
        this.secuenciaLector = secuenciaLector;
    }

    public double getGpsLatitud() {
        return gpsLatitud;
    }

    public void setGpsLatitud(double gpsLatitud) {
        this.gpsLatitud = gpsLatitud;
    }

    public double getGpsLongitud() {
        return gpsLongitud;
    }

    public void setGpsLongitud(double gpsLongitud) {
        this.gpsLongitud = gpsLongitud;
    }

    public int getInstalacionId() {
        return instalacionId;
    }

    public void setInstalacionId(int instalacionId) {
        this.instalacionId = instalacionId;
    }

    public int getMedidorId() {
        return medidorId;
    }

    public void setMedidorId(int medidorId) {
        this.medidorId = medidorId;
    }

    public int getRutaId() {
        return rutaId;
    }

    public void setRutaId(int rutaId) {
        this.rutaId = rutaId;
    }

    public int getTipoDocumentoId() {
        return tipoDocumentoId;
    }

    public void setTipoDocumentoId(int tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getFlagEnvio() {
        return flagEnvio;
    }

    public void setFlagEnvio(int flagEnvio) {
        this.flagEnvio = flagEnvio;
    }
}
