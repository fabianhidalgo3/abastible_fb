package cl.jfcor.abastible_fb.modelos;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class OrdenLectura implements Serializable {
    private int id;
    private String codigo;
    private int secuencia_lector;
    private String direccion;
    private double gpsLatitud;
    private double gpsLongitud;
    private int instalacionId;
    private int medidorId;
    private int clienteId;
    private int rutaId;
    private int tipoLecturaId;
    private int estadoLecturaId;
    private int tarifaId;
    private int color;
    private int flagEnvio;
    private String observacion;
    private double lecturaAnterior;
    private double consumoPromedio;
    private double lecturaActual;
    private int consumoActual;
    private double consumoSuperior;
    private double consumoInferior;
    private long fechaEjecucion;
    private int claveLecturaId;
    private ArrayList<Intento> listaIntentos;
    private int intentos = 0;
    private ArrayList<Fotografia> fotografias;
    private String mensajeFueraDeRango = "";
    private String numeroMedidor;
    /*
    * Constructor paraEnviar información al Servidor
    * */

    public OrdenLectura(int id, String codigo, int secuencia_lector, String direccion, String numeroMedidor, double gpsLatitud, double gpsLongitud, int instalacionId, int medidorId, int clienteId, int rutaId, int tipoLecturaId, int estadoLecturaId, int tarifaId, int color, int flagEnvio, String observacion, double lecturaAnterior, double consumoPromedio, double lecturaActual, int consumoActual, double consumoSuperior, double consumoInferior, long fechaEjecucion, int claveLecturaId, ArrayList<Fotografia> fotografias) {
        this.id = id;
        this.codigo = codigo;
        this.secuencia_lector = secuencia_lector;
        this.direccion = direccion;
        this.numeroMedidor = numeroMedidor;
        this.gpsLatitud = gpsLatitud;
        this.gpsLongitud = gpsLongitud;
        this.instalacionId = instalacionId;
        this.medidorId = medidorId;
        this.clienteId = clienteId;
        this.rutaId = rutaId;
        this.tipoLecturaId = tipoLecturaId;
        this.estadoLecturaId = estadoLecturaId;
        this.tarifaId = tarifaId;
        this.color = color;
        this.flagEnvio = flagEnvio;
        this.observacion = observacion;
        this.lecturaAnterior = lecturaAnterior;
        this.consumoPromedio = consumoPromedio;
        this.lecturaActual = lecturaActual;
        this.consumoActual = consumoActual;
        this.consumoSuperior = consumoSuperior;
        this.consumoInferior = consumoInferior;
        this.fechaEjecucion = fechaEjecucion;
        this.claveLecturaId = claveLecturaId;
        //this.listaIntentos = listaIntentos;
        this.fotografias = fotografias;
    }

    /*
     * Contructor para recibir informacion desde el servidor
     * */

    public OrdenLectura(int id, String codigo, int secuencia_lector, String direccion, String numeroMedidor, double gpsLatitud, double gpsLongitud, int instalacionId, int medidorId, int clienteId, int rutaId, int tipoLecturaId, int estadoLecturaId, int tarifaId, int color, int flagEnvio, String observacion, double lecturaAnterior, double consumoPromedio, double lecturaActual, int consumoActual, double consumoSuperior, double consumoInferior, long fechaEjecucion, int claveLecturaId) {
        this.id = id;
        this.codigo = codigo;
        this.secuencia_lector = secuencia_lector;
        this.direccion = direccion;
        this.numeroMedidor = numeroMedidor;
        this.gpsLatitud = gpsLatitud;
        this.gpsLongitud = gpsLongitud;
        this.instalacionId = instalacionId;
        this.medidorId = medidorId;
        this.clienteId = clienteId;
        this.rutaId = rutaId;
        this.tipoLecturaId = tipoLecturaId;
        this.estadoLecturaId = estadoLecturaId;
        this.color = color;
        this.flagEnvio = flagEnvio;
        this.observacion = observacion;
        this.lecturaAnterior = lecturaAnterior;
        this.consumoPromedio = consumoPromedio;
        this.lecturaActual = lecturaActual;
        this.consumoActual = consumoActual;
        this.consumoSuperior = consumoSuperior;
        this.consumoInferior = consumoInferior;
        this.fechaEjecucion = fechaEjecucion;
        this.claveLecturaId = claveLecturaId;
    }

    public OrdenLectura(int id, String codigo, int secuencia_lector, String direccion,  String numeroMedidor, int instalacionId, int medidorId, int clienteId, int rutaId, int tipoLecturaId, int estadoLecturaId, int color, int flagEnvio, double lecturaAnterior, double consumoInferior, double consumoSuperior) {
        this.id = id;
        this.codigo = codigo;
        this.secuencia_lector = secuencia_lector;
        this.direccion = direccion;
        this.numeroMedidor = numeroMedidor;
        this.instalacionId = instalacionId;
        this.medidorId = medidorId;
        this.clienteId = clienteId;
        this.rutaId = rutaId;
        this.tipoLecturaId = tipoLecturaId;
        this.estadoLecturaId = estadoLecturaId;
        this.color = color;
        this.flagEnvio = flagEnvio;
        this.lecturaAnterior = lecturaAnterior;
        this.consumoInferior = consumoInferior;
        this.consumoSuperior = consumoSuperior;
    }

    public String parseDate() {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = dateFormatter.format(this.fechaEjecucion);
        return fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getSecuencia_lector() {
        return secuencia_lector;
    }

    public void setSecuencia_lector(int secuencia_lector) {
        this.secuencia_lector = secuencia_lector;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getGpsLatitud() {
        return gpsLatitud;
    }

    public void setGpsLatitud(double gpsLatitud) {
        this.gpsLatitud = gpsLatitud;
    }

    public double getGpsLongitud() {
        return gpsLongitud;
    }

    public void setGpsLongitud(double gpsLongitud) {
        this.gpsLongitud = gpsLongitud;
    }

    public int getInstalacionId() {
        return instalacionId;
    }

    public void setInstalacionId(int instalacionId) {
        this.instalacionId = instalacionId;
    }

    public int getMedidorId() {
        return medidorId;
    }

    public void setMedidorId(int medidorId) {
        this.medidorId = medidorId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public int getRutaId() {
        return rutaId;
    }

    public void setRutaId(int rutaId) {
        this.rutaId = rutaId;
    }

    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    public int getTipoLecturaId() {
        return tipoLecturaId;
    }

    public void setTipoLecturaId(int tipoLecturaId) {
        this.tipoLecturaId = tipoLecturaId;
    }

    public int getEstadoLecturaId() {
        return estadoLecturaId;
    }

    public void setEstadoLecturaId(int estadoLecturaId) {
        this.estadoLecturaId = estadoLecturaId;
    }

    public int getTarifaId() {
        return tarifaId;
    }

    public void setTarifaId(int tarifaId) {
        this.tarifaId = tarifaId;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getFlagEnvio() {
        return flagEnvio;
    }

    public void setFlagEnvio(int flagEnvio) {
        this.flagEnvio = flagEnvio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public double getLecturaAnterior() {
        return lecturaAnterior;
    }

    public void setLecturaAnterior(double lecturaAnterior) {
        this.lecturaAnterior = lecturaAnterior;
    }

    public double getConsumoPromedio() {
        return consumoPromedio;
    }

    public void setConsumoPromedio(double consumoPromedio) {
        this.consumoPromedio = consumoPromedio;
    }

    public double getLecturaActual() {
        return lecturaActual;
    }

    public void setLecturaActual(double lecturaActual) {
        this.lecturaActual = lecturaActual;
    }

    public int getConsumoActual() {
        return consumoActual;
    }

    public void setConsumoActual(int consumoActual) {
        this.consumoActual = consumoActual;
    }

    public double getConsumoSuperior() {
        return consumoSuperior;
    }

    public void setConsumoSuperior(double consumoSuperior) {
        this.consumoSuperior = consumoSuperior;
    }

    public double getConsumoInferior() {
        return consumoInferior;
    }

    public void setConsumoInferior(double consumoInferior) {
        this.consumoInferior = consumoInferior;
    }

    public long getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(long fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public int getClaveLecturaId() {
        return claveLecturaId;
    }

    public void setClaveLecturaId(int claveLecturaId) {
        this.claveLecturaId = claveLecturaId;
    }

    public ArrayList<Intento> getListaIntentos() {
        return listaIntentos;
    }

    public void setListaIntentos(ArrayList<Intento> listaIntentos) {
        this.listaIntentos = listaIntentos;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public ArrayList<Fotografia> getFotografias() {
        return fotografias;
    }

    public void setFotografias(ArrayList<Fotografia> fotografias) {
        this.fotografias = fotografias;
    }
    public boolean fueraDeRango(int consumo, ClaveLectura claveLectura )
    {
        //Si la clave no requiere lectura, la lectura siempre esta dentro del rango.
        if(!claveLectura.isRequerido())
            return false;
        //Verifica cuantas veces se ha ingresado la misma lectura
        if( this.consumoActual == consumo)
            this.intentos++;
        else
        {
            this.intentos = 1;
            this.consumoActual = consumo;
        }

        //Verifica cantidad de veces que se repite la misma lectura para considerarla valida
        if( this.intentos == 2){
            return false;
        }

        //Verifica si lectura actual es menor a lectura anterior
        if(this.consumoActual < this.consumoInferior) {
            this.setMensajeFueraDeRango("Lectura ingresada se encuentra bajo el consumo normal");
            return true;
        }

        if(this.consumoActual > this.consumoSuperior) {
            this.setMensajeFueraDeRango("Lectura ingresada con sobreconsumo");
            return true;
        }
        return false;
    }

    public String getMensajeFueraDeRango() {
        return mensajeFueraDeRango;
    }

    public void setMensajeFueraDeRango(String mensajeFueraDeRango) {
        this.mensajeFueraDeRango = mensajeFueraDeRango;
    }

}
