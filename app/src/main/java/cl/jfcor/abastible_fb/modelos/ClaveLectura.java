package cl.jfcor.abastible_fb.modelos;

import java.io.Serializable;

public class ClaveLectura implements Serializable {
    private int id;
    private String codigo;
    private String nombre;
    private int numeroFotografias; //Cantidad de fotografias por clave
    private  boolean requerido; // 0 no requiere lectura / 1 Requiere lectura
    private boolean efectivo; // 0 lectura no efectiva / 1 lectura efectiva
    private boolean factura; // 0 no factura en terreno / 1 Factura en terreno

    public ClaveLectura() {

    }

    public ClaveLectura(int id, String codigo, String nombre, int numeroFotografias, boolean requerido, boolean efectivo, boolean factura) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.numeroFotografias = numeroFotografias;
        this.requerido = requerido;
        this.efectivo = efectivo;
        this.factura = factura;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroFotografias() {
        return numeroFotografias;
    }

    public void setNumeroFotografias(int numeroFotografias) {
        this.numeroFotografias = numeroFotografias;
    }

    public boolean isRequerido() {
        return requerido;
    }

    public void setRequerido(boolean requerido) {
        this.requerido = requerido;
    }

    public int getRequerido(){
        if(this.isRequerido())
            return 1;
        return 0;

    }


    public boolean isEfectivo() {
        return efectivo;
    }

    public void setEfectivo(boolean efectivo) {
        this.efectivo = efectivo;
    }

    public int getEfectivo(){
        if(this.isEfectivo())
            return 1;
        return 0;

    }

    public boolean isFactura() {
        return factura;
    }

    public void setFactura(boolean factura) {
        this.factura = factura;
    }

    public int getFactura(){
        if(this.isFactura())
            return 1;
        return 0;

    }
}
