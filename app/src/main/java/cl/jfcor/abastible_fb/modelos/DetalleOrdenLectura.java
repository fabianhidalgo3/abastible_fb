package cl.jfcor.abastible_fb.modelos;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DetalleOrdenLectura implements Serializable {
    private int id;
    private int ordenLecturaId;
    private double lecturaAnterior;
    private double consumoPromedio;
    private double lecturaActual;
    private int consumoActual;
    private double consumoSuperior;
    private double consumoInferior;
    private long fechaEjecucion;
    private int claveLecturaId;
    private ArrayList<Intento> listaIntentos;
    private int intentos = 0;
    private ArrayList<Fotografia> fotografias;
    private String mensajeFueraDeRango = "";

    // Constructor para enviar ordenes
    public DetalleOrdenLectura(int id, int ordenLecturaId, double lecturaAnterior, double consumoPromedio, double lecturaActual, double consumoSuperior, double consumoInferior, long fechaEjecucion, int claveLecturaId,int consumoActual, ArrayList<Intento> listaIntentos, ArrayList<Fotografia> fotografias) {
        this.id = id;
        this.ordenLecturaId = ordenLecturaId;
        this.lecturaAnterior = lecturaAnterior;
        this.consumoPromedio = consumoPromedio;
        this.lecturaActual = lecturaActual;
        this.consumoSuperior = consumoSuperior;
        this.consumoInferior = consumoInferior;
        this.fechaEjecucion = fechaEjecucion;
        this.claveLecturaId = claveLecturaId;
        this.consumoActual = consumoActual;
        this.listaIntentos = listaIntentos;
        this.fotografias = fotografias;
    }

    // Constructor para recibir ordenes desde la API.
    public DetalleOrdenLectura(int id, int ordenLecturaId, double lecturaAnterior, double consumoPromedio, double consumoSuperior, double consumoInferior) {
        this.id = id;
        this.ordenLecturaId = ordenLecturaId;
        this.lecturaAnterior = lecturaAnterior;
        this.consumoPromedio = consumoPromedio;
        this.consumoSuperior = consumoSuperior;
        this.consumoInferior = consumoInferior;
    }

    public String parseDate() {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = dateFormatter.format(this.fechaEjecucion);
        return fecha;
    }

    public int getConsumoActual() {
        return consumoActual;
    }

    public void setConsumoActual(int consumoActual) {
        this.consumoActual = consumoActual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrdenLecturaId() {
        return ordenLecturaId;
    }

    public void setOrdenLecturaId(int ordenLecturaId) {
        this.ordenLecturaId = ordenLecturaId;
    }

    public double getLecturaAnterior() {
        return lecturaAnterior;
    }

    public void setLecturaAnterior(double lecturaAnterior) {
        this.lecturaAnterior = lecturaAnterior;
    }

    public double getConsumoPromedio() {
        return consumoPromedio;
    }

    public void setConsumoPromedio(double consumoPromedio) {
        this.consumoPromedio = consumoPromedio;
    }


    public double getLecturaActual() {
        return lecturaActual;
    }

    public void setLecturaActual(double lecturaActual) {
        this.lecturaActual = lecturaActual;
    }

    public double getConsumoSuperior() {
        return consumoSuperior;
    }

    public void setConsumoSuperior(double consumoSuperior) {
        this.consumoSuperior = consumoSuperior;
    }

    public double getConsumoInferior() {
        return consumoInferior;
    }

    public void setConsumoInferior(double consumoInferior) {
        this.consumoInferior = consumoInferior;
    }

    public long getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(long fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public int getClaveLecturaId() {
        return claveLecturaId;
    }

    public void setClaveLecturaId(int claveLecturaId) {
        this.claveLecturaId = claveLecturaId;
    }

    public ArrayList<Intento> getListaIntentos() {
        return listaIntentos;
    }

    public void setListaIntentos(ArrayList<Intento> listaIntentos) {
        this.listaIntentos = listaIntentos;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public ArrayList<Fotografia> getFotografias() {
        return fotografias;
    }

    public void setFotografias(ArrayList<Fotografia> fotografias) {
        this.fotografias = fotografias;
    }
    public boolean fueraDeRango(int consumo, ClaveLectura claveLectura )
    {
        //Si la clave no requiere lectura, la lectura siempre esta dentro del rango.
        if(!claveLectura.isRequerido())
            return false;
        //Verifica cuantas veces se ha ingresado la misma lectura
        if( this.consumoActual == consumo)
            this.intentos++;
        else
        {
            this.intentos = 1;
            this.consumoActual = consumo;
        }

        //Verifica cantidad de veces que se repite la misma lectura para considerarla valida
        if( this.intentos == 2){
            return false;
        }

        //Verifica si lectura actual es menor a lectura anterior
        if(this.consumoActual < this.consumoInferior) {
            this.setMensajeFueraDeRango("Lectura ingresada se encuentra bajo el consumo normal");
            return true;
        }

        if(this.consumoActual > this.consumoSuperior) {
            this.setMensajeFueraDeRango("Lectura ingresada con sobreconsumo");
            return true;
        }
        return false;
    }

    public String getMensajeFueraDeRango() {
        return mensajeFueraDeRango;
    }

    public void setMensajeFueraDeRango(String mensajeFueraDeRango) {
        this.mensajeFueraDeRango = mensajeFueraDeRango;
    }


}
