package cl.jfcor.abastible_fb.modelos;

import java.io.Serializable;

public class Cliente implements Serializable {


    private int id;
    private String rut;
    private String numero_cliente;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String direccionCompleta;
    private String calle;
    private String numeroDomicilio;
    private int empresaId;
    private String observacionDomicilio;
    private String gpsLatitud;
    private String gpsLongitud;

    public Cliente(int id, String numero_cliente, String direccionCompleta, String calle, String numeroDomicilio, String observacionDomicilio, String gpsLatitud, String gpsLongitud) {
        this.id = id;
        this.numero_cliente = numero_cliente;
        this.direccionCompleta = direccionCompleta;
        this.calle = calle;
        this.numeroDomicilio = numeroDomicilio;
        this.observacionDomicilio = observacionDomicilio;
        this.gpsLatitud = gpsLatitud;
        this.gpsLongitud = gpsLongitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNumero_cliente() {
        return numero_cliente;
    }

    public void setNumero_cliente(String numero_cliente) {
        this.numero_cliente = numero_cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getDireccionCompleta() {
        return direccionCompleta;
    }

    public void setDireccionCompleta(String direccionCompleta) {
        this.direccionCompleta = direccionCompleta;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroDomicilio() {
        return numeroDomicilio;
    }

    public void setNumeroDomicilio(String numeroDomicilio) {
        this.numeroDomicilio = numeroDomicilio;
    }

    public int getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(int empresaId) {
        this.empresaId = empresaId;
    }

    public String getObservacionDomicilio() {
        return observacionDomicilio;
    }

    public void setObservacionDomicilio(String observacionDomicilio) {
        this.observacionDomicilio = observacionDomicilio;
    }

    public String getGpsLatitud() {
        return gpsLatitud;
    }

    public void setGpsLatitud(String gpsLatitud) {
        this.gpsLatitud = gpsLatitud;
    }

    public String getGpsLongitud() {
        return gpsLongitud;
    }

    public void setGpsLongitud(String gpsLongitud) {
        this.gpsLongitud = gpsLongitud;
    }
}
