package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;
import cl.jfcor.abastible_fb.modelos.Ruta;
import cl.jfcor.abastible_fb.modelos.Usuario;

public class ListaOrdenesCorteReposicionFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterOrdenLectura.OrdenLecturaListener {
    private String tag = "Ordenes de Lectura";
    public final static int INGRESAR_LECTURA = 1;

    private ArrayList<OrdenLectura> ordenes;
    private Ruta ruta;
    private Usuario usuario;
    private Bd bd;
    private Spinner filtro;
    private Spinner orden;
    private EditText buscar;
    private TextView pendientes;
    private int tipoLectura;
    private RecyclerView recicler;
    private String textoBusqueda;
    private int modoBusqueda;

    //private OrdenLecturaDAO lecturaDAO;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lista_ordenes_lecturas, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        this.ruta = (Ruta) args.getSerializable("ruta");
        ((MenuUsuario) getActivity()).setActionBarTitle(this.ruta.getCodigo());//Obtener parametros
        this.usuario = (Usuario) args.getSerializable("usuario");
        this.tipoLectura = args.getInt("tipoLectura");
        this.modoBusqueda = args.getInt("modoBusqueda");
        this.textoBusqueda = args.getString("textoBusqueda");
        Log.d("modoBusquda", Integer.toString(modoBusqueda));
        Log.d("textoBusquda", Integer.toString(modoBusqueda));
        //Se agregan filtros al spinner
        String[] arraySpinner = new String[]{"Dirección", "N° Medidor"};
        String[] arrayOrderBy = new String[]{"Secuencia UL Ascendente", "Secuencia UL Descendente"};
        this.filtro = (Spinner) this.getView().findViewById(R.id.filtroImpresion);
        this.orden = (Spinner) this.getView().findViewById(R.id.spinnerOrden);

        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.filtro.setAdapter(adapter);

        ArrayAdapter<String> adapterOrden = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, arrayOrderBy);
        adapterOrden.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.orden.setAdapter(adapterOrden);
        this.orden.setOnItemSelectedListener(this);

        this.buscar = (EditText) this.getView().findViewById(R.id.txt_busquedaImpresion);
        Button btnBuscar = (Button) this.getView().findViewById(R.id.btn_buscar);
        btnBuscar.setOnClickListener(this);

        TextView txtRuta = (TextView) this.getView().findViewById(R.id.listaOrden_ruta);
        txtRuta.setText("Ruta: " + this.ruta.getCodigo());

        this.pendientes = (TextView) this.getView().findViewById(R.id.listaOrden_pendientes);
        if (modoBusqueda == 1) {
            this.ordenes = this.bd.listarOrdenesMedidor(textoBusqueda, 1, this.getOrderBy());
        }else if (modoBusqueda == 2) {
            this.ordenes = this.bd.listarOrdenesDireccion(textoBusqueda, 1, this.getOrderBy());
        } else {
            this.listarOrdenes();
        }

    }

    public void listarOrdenes() {
        this.bd.abrir();
        this.ordenes = this.bd.leerOrdenes(this.ruta.getId(), this.tipoLectura, getOrderBy());
        recicler = (RecyclerView) this.getView().findViewById(R.id.listaOrdenes);
        recicler.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL,false));
        AdapterOrdenLectura adaptador = new AdapterOrdenLectura(this.ordenes, getActivity(),this);
        recicler.setAdapter(adaptador);
        this.pendientes.setText("Pendientes: " + this.ordenes.size());
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.bd = Bd.getInstance(activity);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // Click al Boton Buscar
    @Override
    public void onClick(View v) {
        this.filtrar();
    }

    // Orden Unidad de lectura por secuencia Lector
    private String getOrderBy() {
        String orden = this.orden.getSelectedItem().toString();
        String orderBy = null;
        if (orden.equals("Secuencia UL Ascendente"))
            orderBy = "secuencia_lector ASC";
        if (orden.equals("Secuencia UL Descendente"))
            orderBy = "secuencia_lector DESC";

        return orderBy;
    }

    // filtra por medidor o por dirección
    private void filtrar() {
        String filtro = this.filtro.getSelectedItem().toString();

        if (filtro.equals("N° Medidor"))
            this.ordenes = this.bd.listarOrdenesMedidor(this.buscar.getText().toString(), 1, this.getOrderBy());
        if (filtro.equals("Dirección")) {
            this.ordenes = this.bd.listarOrdenesDireccion(this.buscar.getText().toString(), 1, this.getOrderBy());
        }
        //Log.d("Ordenes",(String)this.ordenes.);
        //this.ordenes = this.bd.leerOrdenes(this.ruta.getId(), this.tipoLectura, getOrderBy());
        recicler = (RecyclerView) this.getView().findViewById(R.id.listaOrdenes);
        recicler.setLayoutManager(new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false));
        AdapterOrdenLectura adaptador = new AdapterOrdenLectura(this.ordenes, getActivity(),this);
        recicler.setAdapter(adaptador);

        this.pendientes.setText("Pendientes: " + this.ordenes.size());
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (this.buscar.getText().toString() != "")
            this.listarOrdenes();
        else
            this.filtrar();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        return;
    }

    @Override
    public void onOrdenClick(int position) {
        // Llamo al fragment de orden lecturas
        Fragment fragment = null;
        try {
            fragment = OrdenLecturaFragment.class.newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //Parametros que se pasan al fragment
        Bundle args = new Bundle();
        args.putSerializable("orden", this.ordenes.get(position));
        args.putSerializable("usuario", usuario);
        args.putSerializable("posicion", position);
        args.putSerializable("tipolectura", this.tipoLectura);
        args.putSerializable("ruta", this.ruta);
        args.putSerializable("textoBusqueda", this.buscar.getText().toString());
        if (filtro.equals("N° Medidor")) {
            args.putSerializable("modoBusqueda", 1);
            args.putSerializable("textoBusqueda", this.buscar.getText().toString());
        }
        else if (filtro.equals("Dirección")){
            args.putSerializable("modoBusqueda", 2);
            args.putSerializable("textoBusqueda", this.buscar.getText().toString());
        }
        else{
            args.putSerializable("modoBusqueda", 0);
            args.putSerializable("textoBusqueda", 0);
        }

        fragment.setArguments(args);
        android.support.v4.app.FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

}
