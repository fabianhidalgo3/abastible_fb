package cl.jfcor.abastible_fb.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cl.jfcor.abastible_fb.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OrdenConveniosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrdenConveniosFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters

    public OrdenConveniosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdenConveniosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrdenConveniosFragment newInstance(String param1, String param2) {
        OrdenConveniosFragment fragment = new OrdenConveniosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_orden_convenio, container, false);
    }
}
