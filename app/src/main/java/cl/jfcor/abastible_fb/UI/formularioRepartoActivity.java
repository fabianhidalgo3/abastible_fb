package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.OrdenReparto;
import cl.jfcor.abastible_fb.modelos.RutaReparto;
import cl.jfcor.abastible_fb.modelos.Usuario;

public class formularioRepartoActivity extends Fragment implements View.OnClickListener {
    private String tag = "Ordenes de Reparto";
    private ArrayList<OrdenReparto> ordenes;
    private RutaReparto ruta;
    private Usuario usuario;
    private Bd bd;
    private EditText cajaBusqueda;
    private Button btnBuscar;
    private Button btnTerminarReparto;
    private TextView pendientes;
    private TextView visitados;
    private TextView asignados;
    String numeroCliente;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_formulario_reparto, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MenuUsuario) getActivity()).setActionBarTitle("Ordenes de Reparto");//Obtener parametros
        Bundle args = getArguments();
        //Obtengo Elemenotos de la vista
        this.btnBuscar = (Button) this.getView().findViewById(R.id.btnBuscar);
        this.btnTerminarReparto = (Button) this.getView().findViewById(R.id.btnTerminarReparto);
        this.pendientes = (TextView) this.getView().findViewById(R.id.totalPendientes);
        this.visitados = (TextView) this.getView().findViewById(R.id.totalVisitados);
        this.asignados = (TextView) this.getView().findViewById(R.id.totalAsignados);
        this.cajaBusqueda = (EditText) this.getView().findViewById(R.id.caja_busqueda);
        this.bd.abrir();
        //Envio Total Ordenes al Textview
        this.pendientes.setText(Integer.toString(this.bd.totalOrdenesReparto()));
        this.visitados.setText(Integer.toString(this.bd.totalOrdenesRepartoVisitado()));
        int totalAsignado = (this.bd.totalOrdenesReparto()+ this.bd.totalOrdenesRepartoVisitado());
        this.asignados.setText(Integer.toString(totalAsignado));


        this.btnBuscar.setOnClickListener(this);
        this.btnTerminarReparto.setOnClickListener(this);

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.bd = Bd.getInstance(activity);
    }

    @Override
    public void onStart() {
        super.onStart();
    }



    @Override
    public void onClick(View v) {
        LocationManager ubicacion = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location loc = ubicacion.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        // Boton Ingresar
        switch (v.getId()) {
            case R.id.btnBuscar:
                numeroCliente = this.cajaBusqueda.getText().toString();
                if (numeroCliente.equals("")){
                    Toast.makeText(this.getContext(),"Debe Ingresar un numero de cliente",Toast.LENGTH_SHORT).show();
                }else{
                    OrdenReparto  ordenReparto = new OrdenReparto();
                    ordenReparto = this.bd.buscarOrdenReparto(numeroCliente);
                    if (ordenReparto.getDireccion() == null){
                        Toast.makeText(this.getContext(),"No se encuentra el numero de cliente", Toast.LENGTH_SHORT).show();
                        this.cajaBusqueda.setText("");
                    }else{
                        this.bd.abrir ();
                        ordenReparto.setGpsLatitud (loc.getLatitude ());
                        ordenReparto.setGpsLongitud (loc.getLongitude ());
                        ordenReparto.setEstadoRepartoId (4);
                        ordenReparto.setFechaEjecucion (new Date().getTime ());
                        this.bd.actualizarOrdenReparto (ordenReparto);
                        Toast.makeText(this.getContext(),"Cliente Registrado Correctamente", Toast.LENGTH_SHORT).show();
                        this.pendientes.setText(Integer.toString(this.bd.totalOrdenesReparto()));
                        this.visitados.setText(Integer.toString(this.bd.totalOrdenesRepartoVisitado()));
                        this.cajaBusqueda.setText("");
                    }
                }
               break;
            case R.id.btnTerminarReparto:
                mostrarDialogo();
                break;
            default:
                break;
            }
        }


        private void cerrarReparto(){
            LocationManager ubicacion = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location loc = ubicacion.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            this.bd.terminarReparto(loc.getLatitude(),loc.getLongitude(), new  Date().getTime());
            this.pendientes.setText(Integer.toString(this.bd.totalOrdenesReparto()));
            this.visitados.setText(Integer.toString(this.bd.totalOrdenesRepartoVisitado()));
        }

    public void mostrarDialogo(){
        new AlertDialog.Builder(this.getContext())
                .setTitle("Terminar Proceso de Reparto")
                .setMessage("¿Desea Terminar el Proceso de Reparto")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cerrarReparto();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

}


