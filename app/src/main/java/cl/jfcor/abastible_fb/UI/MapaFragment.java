package cl.jfcor.abastible_fb.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.modelos.Cliente;

/**
 * Fragmento utilizado para mostrar el mapa con la ruta generada con los puntos gps de meses anteriores
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "Mapa";
    private Cliente cliente;
    // Declaro variables para crear el mapa
    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        this.cliente = (Cliente) args.getSerializable("cliente");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_mapa, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapView = (MapView) mView.findViewById(R.id.map_view);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.addMarker(
                new MarkerOptions().position(new LatLng(
                        Double.parseDouble(this.cliente.getGpsLatitud()),
                        Double.parseDouble(this.cliente.getGpsLongitud())
                )).title(this.cliente.getNombre())
        );
        CameraPosition positionRecint = CameraPosition.builder().target(
                new LatLng(
                        Double.parseDouble(this.cliente.getGpsLatitud()),
                        Double.parseDouble(this.cliente.getGpsLongitud())
                )).zoom(16).bearing(0).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(positionRecint));
    }
}
