package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.Cliente;
import cl.jfcor.abastible_fb.modelos.Instalacion;
import cl.jfcor.abastible_fb.modelos.Medidor;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;
import cl.jfcor.abastible_fb.modelos.Ruta;

public class AdapterOrdenLectura extends RecyclerView.Adapter<AdapterOrdenLectura.ViewHolderOrdenLectura> {
    private Activity context;
    private ArrayList<OrdenLectura> ordenes;
    private Bd bd = Bd.getInstance(this.context);
    private OrdenLecturaListener mOrdenLecturaListener;


    public AdapterOrdenLectura(ArrayList<OrdenLectura> ordenes, Activity context, OrdenLecturaListener mOrdenLecturaListener) {
        this.ordenes = ordenes;
        this.context = context;
        this.mOrdenLecturaListener = mOrdenLecturaListener;
    }

    @NonNull
    @Override
    public ViewHolderOrdenLectura onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_orden_lectura, null , false);
        return new ViewHolderOrdenLectura(view, mOrdenLecturaListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderOrdenLectura holder, int position) {
        holder.asignarDatos(ordenes.get(position));
        int tipoLectura =  ordenes.get(position).getTipoLecturaId();
        //Color Blanco
        if (ordenes.get(position).getColor() == 0 && tipoLectura == 1){
            holder.fondo.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.direccion.setTextColor((Color.parseColor("#000000")));
            holder.numeroCliente.setTextColor((Color.parseColor("#000000")));
            holder.numeroInstalacion.setTextColor((Color.parseColor("#000000")));
            holder.numeroMedidor.setTextColor((Color.parseColor("#000000")));
            holder.secuenciaLector.setTextColor((Color.parseColor("#000000")));
            holder.ruta.setTextColor((Color.parseColor("#000000")));
            holder.observacion.setTextColor((Color.parseColor("#000000")));
            holder.txtRuta.setTextColor((Color.parseColor("#000000")));
            holder.txtObservacion.setTextColor((Color.parseColor("#000000")));
            holder.txtSecuenciaLector.setTextColor((Color.parseColor("#000000")));
            holder.txtNumeroCliente.setTextColor((Color.parseColor("#000000")));
            holder.txtNumeroMedidor.setTextColor((Color.parseColor("#000000")));
            holder.txtNumeroInstalacion.setTextColor((Color.parseColor("#000000")));
            holder.txtDireccion.setTextColor((Color.parseColor("#000000")));
        }
        //Color Verde > Clientes SEC
        if (ordenes.get(position).getColor() == 1 && tipoLectura == 1){
            holder.fondo.setBackgroundColor(Color.parseColor("#248519"));
            holder.direccion.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroCliente.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroInstalacion.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroMedidor.setTextColor((Color.parseColor("#ffffff")));
            holder.secuenciaLector.setTextColor((Color.parseColor("#ffffff")));
            holder.ruta.setTextColor((Color.parseColor("#ffffff")));
            holder.observacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtRuta.setTextColor((Color.parseColor("#ffffff")));
            holder.txtObservacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtSecuenciaLector.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroCliente.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroMedidor.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroInstalacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtDireccion.setTextColor((Color.parseColor("#ffffff")));
        }
        //Color Amarillo Cliente > Lectura Estimada
        if (ordenes.get(position).getColor() == 2 && tipoLectura == 1){
            holder.fondo.setBackgroundColor(Color.parseColor("#ffc107"));
            holder.direccion.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroCliente.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroInstalacion.setTextColor((Color.parseColor("#ffffff")));
            holder.numeroMedidor.setTextColor((Color.parseColor("#ffffff")));
            holder.secuenciaLector.setTextColor((Color.parseColor("#ffffff")));
            holder.ruta.setTextColor((Color.parseColor("#ffffff")));
            holder.observacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtRuta.setTextColor((Color.parseColor("#ffffff")));
            holder.txtObservacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtSecuenciaLector.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroCliente.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroMedidor.setTextColor((Color.parseColor("#ffffff")));
            holder.txtNumeroInstalacion.setTextColor((Color.parseColor("#ffffff")));
            holder.txtDireccion.setTextColor((Color.parseColor("#ffffff")));

        }
        //Color Azul
        if (ordenes.get(position).getColor() == 3 && tipoLectura == 1){
            holder.fondo.setBackgroundColor(Color.parseColor("#17a2b8"));
        }
        //Color Rojo
        if (ordenes.get(position).getColor() == 4 && tipoLectura == 1){
            holder.fondo.setBackgroundColor(Color.parseColor("#dc3545"));
        }
        if (tipoLectura == 2){
            holder.fondo.setBackgroundColor(Color.parseColor("#dc3545"));
        }
    }

    @Override
    public int getItemCount() {
        return ordenes.size();
    }

    public class ViewHolderOrdenLectura extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView direccion, numeroInstalacion,numeroMedidor, numeroCliente,secuenciaLector, observacion, ruta;
        TextView txtDireccion, txtNumeroInstalacion,txtNumeroMedidor, txtNumeroCliente,txtSecuenciaLector, txtObservacion, txtRuta;
        OrdenLecturaListener ordenLecturaListener;
        LinearLayout fondo;

        public ViewHolderOrdenLectura(@NonNull View itemView, OrdenLecturaListener ordenLecturaListener) {
            super(itemView);
            numeroCliente = itemView.findViewById(R.id.row_orden_numero_cliente);
            numeroInstalacion = itemView.findViewById(R.id.row_orden_numero_instalacion);
            direccion = (TextView) itemView.findViewById(R.id.row_orden_dir);
            numeroMedidor = itemView.findViewById(R.id.row_orden_medidor);
            secuenciaLector = itemView.findViewById(R.id.row_orden_secuencia);
            observacion = itemView.findViewById(R.id.row_orden_observacion_recinto);
            ruta = itemView.findViewById(R.id.row_orden_ruta);
            fondo = itemView.findViewById(R.id.item_lectura);
            txtDireccion = itemView.findViewById(R.id.text_direccion);
            txtNumeroInstalacion = itemView.findViewById(R.id.text_numero_instalacion);
            txtNumeroMedidor = itemView.findViewById(R.id.text_numero_medidor);
            txtNumeroCliente = itemView.findViewById(R.id.text_numero_cliente);
            txtSecuenciaLector = itemView.findViewById(R.id.text_secuencia);
            txtObservacion = itemView.findViewById(R.id.text_observacionRecinto);
            txtRuta = itemView.findViewById(R.id.text_ruta);
            this.ordenLecturaListener = ordenLecturaListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            ordenLecturaListener.onOrdenClick(getAdapterPosition());
        }

        public void asignarDatos(OrdenLectura ordenLectura) {
            bd.abrir();
            Cliente cliente = bd.buscarCliente(ordenLectura.getClienteId());
            Medidor medidor = bd.buscarMedidor(ordenLectura.getMedidorId());
            Instalacion instalacion = bd.buscarInstalacion(ordenLectura.getInstalacionId());
            String secuencia = Integer.toString(ordenLectura.getSecuencia_lector());
            Ruta rutaBuscada = bd.buscarRuta(Integer.toString(ordenLectura.getRutaId()));
            numeroCliente.setText(cliente.getNumero_cliente());
            numeroInstalacion.setText(instalacion.getCodigo());
            direccion.setText(cliente.getDireccionCompleta());
            numeroMedidor.setText(medidor.getNumeroMedidor());
            secuenciaLector.setText(secuencia);
            observacion.setText(medidor.getUbicacionMedidor());
            ruta.setText(rutaBuscada.getCodigo());
        }


    }
    public interface OrdenLecturaListener{
        void onOrdenClick(int position);

    }
}
