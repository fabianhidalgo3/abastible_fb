package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zebra.android.comm.BluetoothPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnectionException;
import com.zebra.android.printer.PrinterLanguage;
import com.zebra.android.printer.ZebraPrinter;
import com.zebra.android.printer.ZebraPrinterFactory;
import com.zebra.android.printer.ZebraPrinterLanguageUnknownException;

import java.io.File;
import java.io.FileOutputStream;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.Cliente;
import cl.jfcor.abastible_fb.modelos.Fotografia;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;
import cl.jfcor.abastible_fb.modelos.ParametrosImpresora;
import cl.jfcor.abastible_fb.modelos.Ruta;
import cl.jfcor.abastible_fb.modelos.Usuario;


public class PaintFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match

    private String tag = "FastMeter";
    private static SimpleDrawingView paint;
    private ZebraPrinterConnection printerConnection;
    OrdenLectura ordenLectura;
    private Fotografia fotografia;
    private Bd bd;
    private int detalleId;
    private EditText rut, nombreRecibe, correo, nroTelefono;
    Cliente cliente;
    private Usuario usuario;
    private int tipoLectura;
    private Ruta ruta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate (R.layout.activity_paint, container, false);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated (savedInstanceState);
        //Obtener argumentos
        Bundle args = getArguments ();
        paint = (SimpleDrawingView) this.getActivity ().findViewById(R.id.lienzo);
        Button clean = (Button)this.getActivity ().findViewById(R.id.button_clean);
        Button save = (Button)this.getActivity ().findViewById(R.id.button_save);
        save.setOnClickListener (this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach (activity);
        this.bd = Bd.getInstance (activity);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.button_clean:
                paint.LimpiarPantalla ();
                break;
            case R.id.button_save:

                guardarClienteRecible(this.rut.getText ().toString (),this.nombreRecibe.getText ().toString (),this.correo.getText ().toString (), this.nroTelefono.getText ().toString ());
                final int idDetalle = this.detalleId;
                try  {
                    paint.setDrawingCacheEnabled(true);
                    Bitmap bitmap = paint.getDrawingCache();
                    File f = null;
                    if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                        File file = new File(Environment.getExternalStorageDirectory(),"FastMeter");
                        if(!file.exists()){
                            file.mkdirs();
                        }
                        f = new File(file.getAbsolutePath()+file.separator+ cliente.getNumero_cliente () +"-FIRMA.jpeg");
                    }
                    FileOutputStream ostream = new FileOutputStream(f);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 10, ostream);
                    ostream.close();
                    this.fotografia = new Fotografia(0, this.detalleId, f.getAbsolutePath(), "", 0);
                    bd.abrir();
                    bd.insertarFotografia(fotografia);
                    //imprimirBoleta ();
                } catch(Exception e){
                    e.printStackTrace();
                }
                paint.destroyDrawingCache();
                this.bd.abrir();
                Bundle args = new Bundle();
                this.tipoLectura = args.getInt("tipoLectura");
                SharedPreferences prefs = this.getActivity().getSharedPreferences("cl.jfcor.fastbilling", Context.MODE_PRIVATE);
                this.usuario = this.bd.buscarUsuario(prefs.getString("cl.jfcor.fastbilling.usuario", ""));
                this.ruta = (Ruta) this.bd.buscarRuta (Integer.toString (this.ordenLectura.getRutaId ()));
                Log.v("Usuario", this.usuario.getEmail ());
                Log.v("Ruta", this.ruta.getCodigo ());
                Fragment fragment = null;
            //    try
              //  {
                   // fragment = ListaOrdenesFragment.class.newInstance();
                //}
               // catch (java.lang.InstantiationException e)
                //{
                  //  e.printStackTrace();
                //}
                //catch (IllegalAccessException e)
                //{
                  //  e.printStackTrace();
                //}

                //Parametros que se pasan al fragment
                args.putSerializable("ruta", this.ruta);
                args.putSerializable("usuario", this.usuario);
                args.putInt("tipoLectura", this.ordenLectura.getTipoLecturaId ());

                fragment.setArguments(args);

                android.support.v4.app.FragmentManager fragmentManager = this.getActivity ().getSupportFragmentManager ();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            default:
                break;

        }


    }


    public void guardarClienteRecible(String rut, String nombre, String correo, String telefono){
        this.bd.abrir ();
        // Actualizo detalle de Orden
        this.ordenLectura.setEstadoLecturaId (4);
        this.bd.actualizarOrden (this.ordenLectura);
        this.bd.cerrar ();
    }

    // Impresion Boucher
    public void imprimirBoleta()
    {
        Log.d("Imprimir", "Boleta");
        Toast.makeText(this.getActivity (),"Imprimir Boleta", Toast.LENGTH_SHORT).show();
        doConnectionTest();
        /*new Thread(new Runnable() {
         public void run() {
         Looper.prepare();
         doConnectionTest();
         Looper.loop();
         Looper.myLooper().quit();
         }
         }).start();*/
    }

    /**
     * Realiza una prueba de coneccion hacia la impresora.
     */
    private void doConnectionTest()
    {
        Log.d("Do", "Connection");
        ZebraPrinter printer = connect();
        if (printer != null) {
            Log.d("Imprimir", "label");
            sendTestLabel();
        } else {
            Log.d("Imprimir", "Null");
            disconnect();
        }
    }

    private void sendTestLabel()
    {
        try
        {
            //String a imprimir -> por ahora fijo, luego se debe armar de forma dinamica
            //Codigo cambiar por documento
            // this.bd.abrir();
            // Cliente cliente = this.bd.buscarCliente(this.ordenLectura.getClienteId());
            // Medidor medidor = this.bd.buscarMedidor(this.ordenLectura.getMedidorId ());
            // Instalacion instalacion = this.bd.buscarInstalacion(this.ordenLectura.getInstalacionId());
            //Ruta ruta = this.bd.buscarRuta(Integer.toString(this.ordenLectura.getRutaId()));
            String imprimir = "! 0 200 200 1200 1\r\n"
                    + "BOX 100 10 700 200 8\r\n"
                    + "CENTER\r\n"
                    + "T 4 0 0 30 R.U.T.: 76.000.739-0\r\n"
                    + "T 4 0 0 80 COMPROBANTE CAMBIO\r\n"
                    + "T 4 0 0 130 N 1\r\n"
                    // + "T 0 3 0 205 S.I.I-SANTIAGO ORIENTE\r\n"
                    + "LEFT\n\r"
                    + "T 0 3 10 250 N CLIENTE: " + "5889323" + "\r\n"
                    + "T 0 3 10 275 Fecha de emision: 11 Febrero 2019\r\n"
                    + "L 10 300 810 300 1\r\n"
                    + "T 0 3 10 310 Sr. (a) " + "Juan Ramirez Perez"  + "\r\n"
                    + "T 0 3 10 335 Direccion de Cambio: " + "Quilpue Sin Numero"  + "\r\n"
                    + "T 0 3 330 360 Quilpue\r\n"
                    + "T 0 3 10 385 Observaciones de Cambio: \r\n"
                    + "T 0 3 10 410 Ruta: 601|"+ ""  + "Cambio Autocontrol Ejecutado\r\n"
                    + "L 10 435 810 435 1\r\n"
                    + "CENTER\r\n"
                    + "T 0 3 0 460 Detalle del Trabajo\r\n"
                    + "LEFT\n\r"
                    + "T 0 3 10 495 N Medidor Retirado\r\n"
                    + "T 0 3 650 495 20053053\r\n"
                    + "T 0 3 10 520 N Medidor Instalado\r\n"
                    + "T 0 3 650 520 5432211\r\n"
                    + "T 0 3 10 575 Lectura Retirada\r\n"
                    + "T 0 3 650 575 545\r\n"
                    + "T 0 3 10 630 Lectura Instalacion\n"
                    + "T 0 3 650 630 0\r\n"
                    + "T 0 3 10 685 Modelo Medidor\r\n"
                    + "T 0 3 650 685 FD1\r\n"
                    + "T 0 3 10 720 Trabajos de Menores\r\n"
                    //+ "B PDF-417 100 1050 XD 5 YD 30 C 3 S 2\r\n"
                    //+ "PDF DATA\r\n"
                    //+ "codigo prueba\r\n"
                    + "ENDPDF\r\n"
                    + "PRINT \r\n";

            printerConnection.write(imprimir.getBytes());

            //setStatus("Sending Data", Color.BLUE);
            //DemoSleeper.sleep(1500);
            if (printerConnection instanceof BluetoothPrinterConnection) {
                String friendlyName = ((BluetoothPrinterConnection) printerConnection).getFriendlyName();
                //setStatus(friendlyName, Color.MAGENTA);
                //DemoSleeper.sleep(500);
            }
        } catch (ZebraPrinterConnectionException e) {
            //setStatus(e.getMessage(), Color.RED);
        } finally {
            disconnect();
            Intent resultIntent = new Intent();
            //setResult(Activity.RESULT_OK, resultIntent);
            //this.popBackStack();
        }
    }

    /**
     * Metodo encargado de conectarse a la impresora
     * @return instancia de la impresora.
     */
    public ZebraPrinter connect()
    {

        this.bd.abrir();
        ParametrosImpresora parametros = this.bd.buscarParametrosImpresora(1);

        //setStatus("Connecting...", Color.YELLOW);
        printerConnection = null;
        printerConnection = new BluetoothPrinterConnection(parametros.getMac());

        //SettingsHelper.saveBluetoothAddress(this, getMacAddressFieldText());
        try {
            printerConnection.open();
            //setStatus("Connected", Color.GREEN);
        }   catch (ZebraPrinterConnectionException e) {
            e.printStackTrace();
            //setStatus("Comm Error! Disconnecting", Color.RED);
            //DemoSleeper.sleep(1000);
            Log.d("Error", "conection.open");
            disconnect();
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(printerConnection);
                //setStatus("Determining Printer Language", Color.YELLOW);
                PrinterLanguage pl = printer.getPrinterControlLanguage();
                //setStatus("Printer Language " + pl, Color.BLUE);
            } catch (ZebraPrinterConnectionException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                //DemoSleeper.sleep(1000);
                Log.d("Error", "conection exception");
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                //DemoSleeper.sleep(1000);
                Log.d("Error", "printerlenguage");
                disconnect();
            }
        }

        return printer;
    }

    /**
     * Cierra la coneccion con la impresora
     */
    public void disconnect()
    {
        try
        {
            //setStatus("Disconnecting", Color.RED);
            if (printerConnection != null)
            {
                printerConnection.close();
            }
            //setStatus("Not Connected", Color.RED);
        }
        catch (ZebraPrinterConnectionException e)
        {
            //setStatus("COMM Error! Disconnected", Color.RED);
        }
        finally
        {
            //enableTestButton(true);
        }
    }
}
