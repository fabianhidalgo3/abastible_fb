package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.Cliente;
import cl.jfcor.abastible_fb.modelos.Instalacion;
import cl.jfcor.abastible_fb.modelos.Medidor;
import cl.jfcor.abastible_fb.modelos.OrdenReparto;

/**
 * Una fila de la lista de ordenes de lectura
 * Created by Fabián on 12/6/16.
 */
public class RowOrdenReparto extends ArrayAdapter<OrdenReparto> {
    private Activity context;
    private ArrayList<OrdenReparto> ordenes;
    private Bd bd = Bd.getInstance(this.getContext());
    private LinearLayout container;

    static class ViewHolder {
        public TextView direccion;
        public TextView numeroInstalacion;
        public TextView numeroMedidor;
        public TextView numeroCliente;
        public TextView secuenciaLector;
        public TextView observacion;
    }

    public RowOrdenReparto(ArrayList<OrdenReparto> ordenes, Activity context) {
        super(context, R.layout.row_orden, ordenes);
        this.context = context;
        this.ordenes = ordenes;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.row_orden, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.numeroCliente = (TextView) row.findViewById(R.id.row_orden_numero_cliente);
            viewHolder.numeroInstalacion = (TextView) row.findViewById(R.id.row_orden_numero_instalacion);
            viewHolder.direccion = (TextView) row.findViewById(R.id.row_orden_dir);
            viewHolder.numeroMedidor = (TextView) row.findViewById(R.id.row_orden_medidor);
            viewHolder.secuenciaLector = (TextView) row.findViewById(R.id.row_orden_secuencia);
            viewHolder.observacion = (TextView) row.findViewById(R.id.row_orden_observacion_recinto);

            row.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) row.getTag();
        OrdenReparto orden = this.ordenes.get(position);
        this.bd.abrir();
        //Se obtienen datos a mostrar y se setean en las variables
        Cliente cliente = this.bd.buscarCliente(orden.getClienteId());
        Medidor medidor = this.bd.buscarMedidor(orden.getMedidorId());
        Instalacion instalacion = this.bd.buscarInstalacion(orden.getInstalacionId());
        String secuencia = Integer.toString(orden.getSecuenciaLector());
        holder.secuenciaLector.setText(secuencia);


        // SE VALIDAN ALGUNOS CAMPOS QUE PODRIAN SER NULOS
        if (cliente != null) {
            holder.numeroCliente.setText(cliente.getNumero_cliente());
            holder.direccion.setText(cliente.getDireccionCompleta());
        }
        else {
            holder.numeroCliente.setText("");
            holder.direccion.setText("");
        }

        if (medidor != null) {
            holder.numeroMedidor.setText(medidor.getNumeroMedidor());
            holder.observacion.setText(medidor.getUbicacionMedidor());
        } else{
            holder.numeroMedidor.setText("");
            holder.observacion.setText("");
        }

        if (instalacion != null) {
            holder.numeroInstalacion.setText(instalacion.getCodigo());
        } else {
            holder.numeroInstalacion.setText("");
        }

        return row;


    }



}
