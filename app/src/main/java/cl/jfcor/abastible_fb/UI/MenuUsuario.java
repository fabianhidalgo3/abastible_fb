package cl.jfcor.abastible_fb.UI;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.Usuario;

public class MenuUsuario extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Bd bd;
    private Usuario usuario;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_usuario);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setActionBarTitle("Abastible");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Inicializa sincronizacion con servidor
        SharedPreferences prefs = this.getSharedPreferences("cl.jfcor.abastible_fb", Context.MODE_PRIVATE);
        this.bd = Bd.getInstance(this);
        this.bd.abrir();
        Usuario usuario = this.bd.buscarUsuario(prefs.getString("cl.jfcor.abastible_fb.usuario", ""));
        new SincronizacionServidor(usuario, this).execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_usuario, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle navigation view item clicks.

        Fragment fragment = null;
        Class fragmentClass;
        Bundle args = new Bundle();

        switch (menuItem.getItemId()) {
            case R.id.nav_rutas:
                args.putSerializable("tipoLectura", 1);
                fragmentClass = RutasLecturaFragment.class;
                new SincronizacionServidor(usuario, this).execute();
                break;

            case R.id.nav_convenios:
                args.putSerializable("tipoLectura", 2);
                fragmentClass = PaintFragment.class;
                break;
            case R.id.nav_repartos:
                fragmentClass = formularioRepartoActivity.class;
                break;

            default:
                args.putSerializable("tipoLectura", 1);
                fragmentClass = RutasLecturaFragment.class;
                new SincronizacionServidor(usuario, this).execute();
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragment.setArguments(args);
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void setActionBarTitle(String title) {
        this.getSupportActionBar().setTitle(title);
    }
}
