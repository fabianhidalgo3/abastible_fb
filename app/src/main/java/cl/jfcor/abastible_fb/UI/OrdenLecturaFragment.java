package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.ClaveLectura;
import cl.jfcor.abastible_fb.modelos.Cliente;
import cl.jfcor.abastible_fb.modelos.Instalacion;
import cl.jfcor.abastible_fb.modelos.Intento;
import cl.jfcor.abastible_fb.modelos.Medidor;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;


public class OrdenLecturaFragment extends Fragment implements View.OnClickListener , AdapterView.OnItemSelectedListener{
    private OrdenLectura ordenLectura;
    public Cliente cliente;
    private EditText txtLectura;
    private Spinner claves;
    private Bd bd;
    private static final String TAG = "Cliente Pendientes";
    private int modoBusqueda;
    private String textoBusqueda;
    //TODO: Mover funciones de impresion a otra vista.
    //TODO: Revisar intent result en fragment.
    //TODO: Refactorizar, Documentar.

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_orden_lectura, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments ();
        this.ordenLectura = (OrdenLectura) args.getSerializable ("orden");
        this.modoBusqueda = args.getInt("modoBusqueda");
        this.textoBusqueda = args.getString("textoBusqueda");
        String lecturaAnterior = Double.toString(this.ordenLectura.getLecturaAnterior());
        this.bd.abrir ();
        Cliente cliente = this.bd.buscarCliente (this.ordenLectura.getClienteId ());
        Medidor medidor = this.bd.buscarMedidor (this.ordenLectura.getMedidorId ());
        Instalacion instalacion = this.bd.buscarInstalacion (this.ordenLectura.getInstalacionId ());
        ((MenuUsuario) getActivity()).setActionBarTitle(medidor.getNumeroMedidor());//Obtener parametros
        TextView txtNumCliente = (TextView) this.getActivity ().findViewById (R.id.ordenLectura_Cliente);
        TextView txtdireccion = (TextView) this.getActivity ().findViewById (R.id.ordenLectura_Direccion);
        TextView txtNumMedidor = (TextView) this.getActivity ().findViewById (R.id.ordenLectura_numMedidor);
        TextView txtNumInstalacion = (TextView) this.getActivity ().findViewById (R.id.ordenLectura_numInstalacion);
        TextView txtObservacion = (TextView) this.getActivity ().findViewById (R.id.ordenLectura_Observacion);
        this.txtLectura = (EditText) this.getActivity ().findViewById (R.id.ordenLectura_lectura);
        txtNumCliente.setText (cliente.getNumero_cliente ());
        txtdireccion.setText(cliente.getDireccionCompleta());
        txtNumMedidor.setText(medidor.getNumeroMedidor());
        txtNumInstalacion.setText(instalacion.getCodigo());
        txtObservacion.setText(medidor.getUbicacionMedidor());
        //Obtener argumentos
        ArrayList<ClaveLectura> claves = this.bd.leerClaves ();
        this.claves = (Spinner) this.getActivity ().findViewById (R.id.spinner_claves);
        this.claves.setAdapter (new SpinAdapterClaves (this.getContext (), android.R.layout.simple_spinner_dropdown_item, claves));
        this.claves.setOnItemSelectedListener (this);
        Button grabar = (Button) this.getActivity ().findViewById (R.id.save_lectura);
        grabar.setOnClickListener (this);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.bd = Bd.getInstance(activity);
    }

    @Override
    public void onClick(View view) {
        //Capturo teclado y lo oculto
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        final ClaveLectura clave = (ClaveLectura) this.claves.getSelectedItem();
        String lectura = this.txtLectura.getText().toString();
        if (clave.getId() != 1) {
             lectura = "0";
        }
        double lecturaActual = Double.parseDouble(lectura);
        int lecturaAnterior = 0;
        lecturaAnterior =  (int)Math.round(this.ordenLectura.getLecturaAnterior());
        int consumo = (int)Math.round(lecturaActual) - lecturaAnterior;
        this.ordenLectura.setConsumoActual(consumo);

        this.bd.abrir();
        this.bd.actualizarDetalleOrden(this.ordenLectura);
        if (!this.ordenLectura.fueraDeRango(consumo, clave) || clave.getId() != 1) {
            this.bd.abrir ();
            this.bd.insertarIntento (new Intento(0, this.ordenLectura.getId (), lecturaActual, 0));
            this.guardarLectura(lecturaActual, clave, consumo);
        }
        else {
            this.bd.insertarIntento (new Intento(0, this.ordenLectura.getId (), lecturaActual, 0));
            //Lectura fuera de rango.
            AlertDialog.Builder dialogo = new AlertDialog.Builder (this.getContext ());
            dialogo.setMessage ("Lectura Fuera de Rango");
            dialogo.setMessage (this.ordenLectura.getMensajeFueraDeRango () + " , n° intentos " + Integer.toString (this.ordenLectura.getIntentos ()));
            dialogo.setCancelable (true);
            dialogo.setPositiveButton ("Aceptar", new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel ();
                }
            });
            AlertDialog alerta = dialogo.create ();
            alerta.show ();
        }
    }


    /**
     * Valida que se ingreso lectura para claves en que es requerida.
     *
     * @param v            Valor del campo de lectura
     * @param claveLectura Clave Lectura seleccionada en spinner
     * @return boolean
     */
    private boolean validarLectura(String v, ClaveLectura claveLectura) {
        if (!claveLectura.isRequerido() && v.isEmpty())
            return true;
        else if (!v.isEmpty()) {
            return true;
        }
        return false;
    }



    /**
     * Valida que se haya ingresado observacion.
     *
     * @param claveLectura Clave seleccionada en spinner
     * @return boolean
     */
    private boolean validarObservacion(ClaveLectura claveLectura) {
        return true;
    }


    private int valida = 0;


    public void enviarUbicacion(Cliente cliente) {
        Fragment fragment = null;
        try {
            fragment = MapaFragment.class.newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //Parametros que se pasan al fragment
        Bundle args = new Bundle();
        args.putSerializable("cliente", cliente);
        fragment.setArguments(args);
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }


    private void guardarLectura(Double lectura, ClaveLectura clave, Integer consumo) {
        //Obtiene posicion gps al momento de guardar lectura
        LocationManager ubicacion = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location loc = ubicacion.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Log.d(TAG, ubicacion.toString());
        if (ubicacion != null) {
            this.ordenLectura.setGpsLatitud(loc.getLatitude());
            this.ordenLectura.setGpsLongitud(loc.getLongitude());
        }
        else{
            Toast.makeText(this.getContext(), "Debe Activar la Ubicación GPS", Toast.LENGTH_SHORT).show();
        }
        //Se actualizan los datos del numerador
        this.ordenLectura.setLecturaActual(lectura);
        this.ordenLectura.setClaveLecturaId(clave.getId());
        this.ordenLectura.setFechaEjecucion(new Date().getTime());
        this.bd.abrir();
        //this.bd.actualizarDetalleOrden(this.ordenLectura);
        this.ordenLectura.setEstadoLecturaId(4);
        this.bd.actualizarOrden(this.ordenLectura);
        if (clave.getId() != 1 || this.ordenLectura.fueraDeRango(consumo, clave) || this.ordenLectura.getColor() != 0){
            // se toma fotografia
            Fragment fragment = null;
            try {
                fragment = CamaraLecturaFragment.class.newInstance();
            } catch (java.lang.InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            //Parametros que se pasan al fragment
            Bundle args = new Bundle();
            args.putSerializable("orden_lectura", this.ordenLectura);
            args.putSerializable("cantidadFotografias", 1);
            args.putSerializable("modoBusqueda", modoBusqueda);
            args.putSerializable("textoBusqueda", textoBusqueda);
            fragment.setArguments(args);
            FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        }else {
            this.getActivity().onBackPressed();
            // Reenviar a lista de ordenes de trabajo filtrando las ordenes de lectura
        }

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}


