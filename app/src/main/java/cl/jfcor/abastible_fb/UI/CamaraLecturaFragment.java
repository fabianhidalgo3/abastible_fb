package cl.jfcor.abastible_fb.UI;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cl.jfcor.abastible_fb.R;
import cl.jfcor.abastible_fb.base_datos.Bd;
import cl.jfcor.abastible_fb.modelos.CamaraPreview;
import cl.jfcor.abastible_fb.modelos.Fotografia;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;
import cl.jfcor.abastible_fb.modelos.Ruta;
import cl.jfcor.abastible_fb.modelos.Usuario;


public class CamaraLecturaFragment extends Fragment implements View.OnClickListener, Camera.PictureCallback {
    private static final int MEDIA_TYPE_IMAGE = 1;
    private Camera mCamara;
    private CamaraPreview mCamaraPreview;
    private FrameLayout preview;
    private Fotografia fotografia;
    private int cantidadFotografias;
    private int fotografiaActual;
    private TextView contador;
    private int ordenLecturaId;
    private Bd bd;
    private int posicion;
    private Ruta ruta;
    private int tipoLectura;
    private String nombreFotografia;
    OrdenLectura ordenLectura;
    private int modoBusqueda;
    private String textoBusqueda;
    private ArrayList<Ruta> rutas;
    private Usuario usuario;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_camara, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Se obtienen datos.
        Bundle args = getArguments();
        this.ordenLectura = (OrdenLectura) args.getSerializable("orden_lectura");
        this.modoBusqueda = args.getInt("modoBusqueda");
        this.textoBusqueda = args.getString("textoBusqueda");
        this.ordenLecturaId = this.ordenLectura.getId();

        this.fotografiaActual = 1;
        this.cantidadFotografias = args.getInt("cantidadFotografias");
        this.contador = (TextView) this.getActivity().findViewById(R.id.numero_fotografia);

        if (this.contador != null)
            this.contador.setText("Nº Medidor | Fotografia " + this.fotografiaActual + "/" + this.cantidadFotografias);

        // Se crea una instancia de camara
        this.mCamara = getCameraInstance();

        // Se crea la vista previa de la camara y se pone el contenido dentro del activity.
        this.mCamaraPreview = new CamaraPreview(this.getContext(), this.mCamara);
        this.preview = (FrameLayout) this.getActivity().findViewById(R.id.camera_preview);
        this.preview.addView(this.mCamaraPreview);
        Button tomarFoto = (Button) this.getActivity().findViewById(R.id.button_capture);
        tomarFoto.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.bd = Bd.getInstance(activity);
    }

    public Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void onClick(View v) {
        //Aqui se controla la captura de la imagen
        Button tomarFoto = (Button) this.getActivity().findViewById(R.id.button_capture);
        tomarFoto.setText("Guardando Fotografía....");
        tomarFoto.setEnabled(false);
        this.mCamara.takePicture(null, null, this);



    }


    private void guardarFotografia() {  //Se obtienen datos.
        //TODO: Guardar las fotografias y actualizar lectura al aceptar la ultima fotografia
        //Se guarda la ruta de la fotografia en la base de datos.
        this.bd.abrir();
        bd.insertarFotografia(this.fotografia);

        //this.getActivity().getSupportFragmentManager().popBackStack();
        // Actualizo nuevamente la lista de ordenes de lectura con modo busqueda y texto Busqueda
        Fragment fragment = null;
        try {
            fragment = ListaOrdenesLecturaFragment.class.newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //Parametros que se pasan al fragment
        Bundle args = new Bundle();
        args.putSerializable("ruta", this.bd.buscarRuta(Integer.toString(this.ordenLectura.getRutaId())));
        args.putSerializable("usuario", this.usuario);
        args.putInt("tipoLectura", this.tipoLectura);
        args.putInt("modoBusqueda", modoBusqueda);
        args.putString("textoBusqueda", textoBusqueda);
        Toast.makeText(this.getContext(),modoBusqueda, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getContext(),textoBusqueda, Toast.LENGTH_SHORT).show();

        fragment.setArguments(args);
        android.support.v4.app.FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File archivo = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (archivo == null) {
            Log.d("Fotografia", "Error creando imagen, revisar permisos de almacenamiento");
            return;
        }

        try {
            FileOutputStream fos = new FileOutputStream(archivo);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("Fotografia", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Fotografia", "Error accessing file: " + e.getMessage());
        }
        this.fotografia = new Fotografia(0, this.ordenLectura.getId(), archivo.getAbsolutePath(), "", 0);
        this.guardarFotografia();
    }

    /**
     * Create a File for saving an image or video
     */
    @Nullable
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "AbastibleFB");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Fastbiling", "Error al Crear la Carpeta de Fotografías");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE)
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        else
            return null;

        return mediaFile;
    }

}
