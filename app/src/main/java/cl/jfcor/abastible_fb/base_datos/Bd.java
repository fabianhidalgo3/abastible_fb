package cl.jfcor.abastible_fb.base_datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.util.ArrayList;

import cl.jfcor.abastible_fb.modelos.ClaveLectura;
import cl.jfcor.abastible_fb.modelos.Cliente;
import cl.jfcor.abastible_fb.modelos.DetalleOrdenLectura;
import cl.jfcor.abastible_fb.modelos.OrdenReparto;
import cl.jfcor.abastible_fb.modelos.RutaReparto;
import cl.jfcor.abastible_fb.modelos.Tarifa;
import cl.jfcor.abastible_fb.modelos.Fotografia;
import cl.jfcor.abastible_fb.modelos.Instalacion;
import cl.jfcor.abastible_fb.modelos.Intento;
import cl.jfcor.abastible_fb.modelos.Medidor;
import cl.jfcor.abastible_fb.modelos.OrdenLectura;
import cl.jfcor.abastible_fb.modelos.ParametrosImpresora;
import cl.jfcor.abastible_fb.modelos.ParametrosServidor;
import cl.jfcor.abastible_fb.modelos.Ruta;
import cl.jfcor.abastible_fb.modelos.TipoDocumento;
import cl.jfcor.abastible_fb.modelos.Usuario;
import static android.provider.BaseColumns._ID;

/**
 * Base de datos embebida
 */
public class Bd extends SQLiteOpenHelper {

    private static final String NOMBRE_BD = "abastible_db";
    private static final int VERSION_ANTERIOR_BD = 6; //Version anterior de la base de datos.
    private static final int VERSION_BD = 7; //Version actual

    private static Bd instanciaBd;

    /**
     * Se obtiene instancia de la base de datos
     *
     * @param context
     * @return
     */
    public static synchronized Bd getInstance(Context context) {
        //Se utiliza un patron de diseño singleton, por lo cual siempre existira una sola instancia
        //de la base de datos en tiempo de ejecucion.
        if (instanciaBd == null) {
            instanciaBd = new Bd(context.getApplicationContext());
        }
        return instanciaBd;
    }

    /**
     * Crea una nueva instancia de la base de datos
     *
     * @param ctx Contexto
     */
    private Bd(Context ctx) {
        super(ctx, NOMBRE_BD, null, VERSION_BD);
    }

    /**
     * Metodo donde se definen las tablas de la base de datos y luego se crean.
     *
     * @param db Base de datos
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String usuario = "CREATE TABLE usuarios (" +
                _ID + " INTEGER PRIMARY KEY," +
                "email TEXT," +
                "password TEXT," +
                "token TEXT," +
                "perfil_id INTEGER)";

        String ruta = "CREATE TABLE ruta(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "nombre TEXT," +
                "usuario INTEGER)";

        String ordenLectura = "CREATE TABLE ordenlectura(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "secuencia_lector INTEGER," +
                "direccion TEXT," +
                "gps_latitud REAL," +
                "gps_longitud REAL," +
                "instalacion_id INTEGER," +
                "cliente_id INTEGER," +
                "ruta_id INTEGER," +
                "tipo_lectura_id INTEGER," +
                "estado_lectura_id INTEGER," +
                "tarifa_id INTEGER, " +
                "color INTEGER," +
                "medidor_id INTEGER," +
                "flag_envio INTEGER," +
                "lectura_anterior REAL," +
                "consumo_promedio REAL," +
                "lectura_actual REAL," +
                "consumo_superior REAL," +
                "consumo_inferior REAL," +
                "consumo_actual INTEGER," +
                "fecha_ejecucion TEXT," +
                "clave_lectura_id INTEGER," +
                "numero_medidor TEXT," +
                "observacion TEXT)";
        //String indexOrdenLectura = "CREATE UNIQUE INDEX idx_orden_lectura_rutaId ON ordenlectura (ruta_id)";

        String detalleOrdenLectura = "CREATE TABLE detalleordenlectura(" +
                _ID + " INTEGER PRIMARY KEY," +
                "orden_lectura_id INTEGER," +
                "numerador_id INTEGER," +
                "lectura_anterior REAL," +
                "consumo_promedio REAL," +
                "lectura_actual REAL," +
                "consumo_superior REAL," +
                "consumo_inferior REAL," +
                "consumo_actual INTEGER," +
                "fecha_ejecucion TEXT," +
                "clave_lectura_id INTEGER)";

        String ordenReparto = "CREATE TABLE ordenreparto(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "estado_reparto_id INTEGER," +
                "fecha_ejecucion TEXT," +
                "secuencia_lector INTEGER," +
                "gps_latitud REAL," +
                "gps_longitud REAL," +
                "instalacion_id INTEGER," +
                "medidor_id INTEGER," +
                "ruta_reparto_id INTEGER," +
                "cliente_id INTEGER," +
                "direccion TEXT," +
                "numero_cliente TEXT," +
                "flag_envio INTEGER," +
                "tipo_documento_id INTEGER)";

        String claveLectura = "CREATE TABLE clavelectura(" +
                _ID + " INTEGER PRIMARY KEY," +
                "clave TEXT," +
                "nombre TEXT," +
                "codigo TEXT," +
                "numero_fotografias INTEGER," +
                "requerido INTEGER," +
                "factura INTEGER," +
                "efectivo INTEGER)";

        String observacion = "CREATE TABLE observacion(" +
                _ID + " INTEGER PRIMARY KEY," +
                "descripcion TEXT)";

        String perfiles = "CREATE TABLE perfiles(" +
                _ID + " INTEGER PRIMARY KEY," +
                "nombre TEXT," +
                "imagen TEXT)";

        String equipos = "CREATE TABLE equipos(" +
                _ID + " INTEGER PRIMARY KEY," +
                "nombre TEXT," +
                "mac TEXT)";

        String estadoLectura = "CREATE TABLE estado_lectura(" +
                _ID + " INTEGER PRIMARY KEY," +
                "nombre TEXT)";

        String instalacion = "CREATE TABLE instalacion(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT)";

        String fotografia = "CREATE TABLE fotografia(" +
                _ID + " INTEGER PRIMARY KEY," +
                "orden_lectura_id INTEGER," +
                "archivo TEXT," +
                "descripcion TEXT," +
                "flag_envio INTEGER)";

        String tipoLectura = "CREATE TABLE tipo_lectura(" +
                _ID + " INTEGER PRIMARY KEY," +
                "nombre TEXT)";


        String tarifa = "CREATE TABLE tarifa(" +
                _ID + " INTEGER PRIMARY KEY," +
                "subempresa_id INTEGER," +
                "sector_id INTEGER," +
                "tipo_sector_id INTEGER," +
                "tipo_tarifa_id INTEGER," +
                "cargo_fijo REAL," +
                "cargo_unico REAL," +
                "cargo_energia_base REAL," +
                "cargo_energia_adicional REAL," +
                "cargo_energia_inyectada REAL," +
                "nombre TEXT)";

        String cliente = "CREATE TABLE cliente(" +
                _ID + " INTEGER PRIMARY KEY," +
                "rut TEXT," +
                "nombre TEXT," +
                "apellido_paterno TEXT," +
                "apellido_materno TEXT," +
                "direccion_completa TEXT," +
                "giro TEXT," +
                "calle TEXT," +
                "numero_domicilio TEXT," +
                "gps_latitud TEXT," +
                "gps_longitud TEXT," +
                "observacion_domicilio TEXT," +
                "numero_cliente TEXT)";


        String medidor = "CREATE TABLE medidor(" +
                _ID + " INTEGER PRIMARY KEY," +
                "numero_medidor TEXT," +
                "numero_digitos TEXT," +
                "diametro INTEGER," +
                "ano INTEGER," +
                "ubicacion_medidor TEXT," +
                "modelo_medidor_id INTEGER)";


        String intentos = "CREATE TABLE intento(" +
                _ID + " INTEGER PRIMARY KEY," +
                "detalle_orden_lectura_id INTEGER," +
                "lectura REAL," +
                "flag_envio INTEGER)";


        String parametros = "CREATE TABLE parametros(" +
                _ID + " INTEGER PRIMARY KEY," +
                "ip TEXT," +
                "intervalo INTEGER)";

        String parametrosImpresora = "CREATE TABLE parametrosimpresora(" +
                _ID + " INTEGER PRIMARY KEY," +
                "mac TEXT," +
                "flag_facturacion INTEGER)";

        String tipoDocumentos = "CREATE TABLE tipo_documentos(" +
                _ID + " INTEGER PRIMARY KEY," +
                "nombre TEXT)";

        String rutaReparto = "CREATE TABLE rutareparto(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "nombre TEXT," +
                "usuario INTEGER)";

        String ordenConvenio = "CREATE TABLE rutareparto(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "nombre TEXT," +
                "usuario INTEGER)";


        String rutaConvenio = "CREATE TABLE rutareparto(" +
                _ID + " INTEGER PRIMARY KEY," +
                "codigo TEXT," +
                "nombre TEXT," +
                "usuario INTEGER)";






        // Ejecucion de sentencias SQL definidas anteriormente
        db.execSQL(usuario);
        db.execSQL(ruta);
        db.execSQL(ordenLectura);
        db.execSQL(ordenReparto);
        db.execSQL(detalleOrdenLectura);
        db.execSQL(claveLectura);
        db.execSQL(observacion);
        db.execSQL(perfiles);
        db.execSQL(equipos);
        db.execSQL(estadoLectura);
        db.execSQL(instalacion);
        db.execSQL(fotografia);
        db.execSQL(tipoLectura);
        db.execSQL(tarifa);
        db.execSQL(cliente);
        db.execSQL(medidor);
        db.execSQL(intentos);
        db.execSQL(parametros);
        db.execSQL(parametrosImpresora);
        db.execSQL(rutaReparto);
        db.execSQL(tipoDocumentos);

        //Insersion de usuario por defecto en la bd.
        db.execSQL("INSERT INTO usuarios (" + _ID + ", email, password, token, perfil_id) values(1,'admin','admin','',1)");
        db.execSQL("INSERT INTO parametrosimpresora(" + _ID + ", mac, flag_facturacion) values(1, '', 0)");

    }

    /**
     * Actualiza la base de datos local cuando hay cambios
     *
     * @param db Base de datos
     * @param i  Version anterior
     * @param i1 version Actual
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS usuarios");
        db.execSQL("DROP TABLE IF EXISTS ruta");
        db.execSQL("DROP TABLE IF EXISTS ordenlectura");
        db.execSQL("DROP TABLE IF EXISTS ordenreparto");
        db.execSQL("DROP TABLE IF EXISTS rutareparto");
        db.execSQL("DROP TABLE IF EXISTS tipo_documentos");
        db.execSQL("DROP TABLE IF EXISTS detalleordenlectura");
        db.execSQL("DROP TABLE IF EXISTS clavelectura");
        db.execSQL("DROP TABLE IF EXISTS observacion");
        db.execSQL("DROP TABLE IF EXISTS perfiles");
        db.execSQL("DROP TABLE IF EXISTS equipos");
        db.execSQL("DROP TABLE IF EXISTS estado_lectura");
        db.execSQL("DROP TABLE IF EXISTS instalacion");
        db.execSQL("DROP TABLE IF EXISTS fotografia");
        db.execSQL("DROP TABLE IF EXISTS tipo_lectura");
        db.execSQL("DROP TABLE IF EXISTS tarifa");
        db.execSQL("DROP TABLE IF EXISTS cliente");
        db.execSQL("DROP TABLE IF EXISTS medidor");
        db.execSQL("DROP TABLE IF EXISTS intento");
        db.execSQL("DROP TABLE IF EXISTS parametros");
        db.execSQL("DROP TABLE IF EXISTS parametrosimpresora");
        onCreate(db);
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                         CLIENTES                                           //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta un cliente
     *
     * @param cliente
     */
    public void insertarCliente(Cliente cliente) {
        if (!this.existeCliente(cliente)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, cliente.getId());
            valores.put("numero_cliente", cliente.getNumero_cliente());
            valores.put("calle", cliente.getCalle());
            valores.put("numero_domicilio", cliente.getNumeroDomicilio());
            valores.put("direccion_completa", cliente.getDireccionCompleta());
            valores.put("gps_latitud", cliente.getGpsLatitud());
            valores.put("gps_longitud", cliente.getGpsLongitud());
            valores.put("observacion_domicilio", cliente.getObservacionDomicilio());

            this.getWritableDatabase().insert("cliente", null, valores);
        }
    }

    /**
     * Busca un cliente por su id
     *
     * @param idCliente identificador de cliente
     * @return
     */
    public Cliente buscarCliente(int idCliente) {
        Cliente resultado = null;
        String[] campos = new String[]{_ID, "direccion_completa", "calle", "numero_domicilio","numero_cliente", "gps_latitud", "gps_longitud", "observacion_domicilio"};
        String[] args = new String[]{Integer.toString(idCliente)};

        Cursor c = this.getReadableDatabase().query("cliente", campos, _ID + "=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int direccionCompleta = c.getColumnIndex("direccion_completa");
        int calle = c.getColumnIndex("calle");
        int numeroDomicilio = c.getColumnIndex("numero_domicilio");
        int numeroCliente = c.getColumnIndex("numero_cliente");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int observacionDomicilio = c.getColumnIndex("observacion_domicilio");
        while (c.moveToNext()) {
            resultado = new Cliente(c.getInt(id),
                    c.getString(numeroCliente),
                    c.getString(direccionCompleta),
                    c.getString(calle),
                    c.getString(numeroDomicilio),
                    c.getString(observacionDomicilio),
                    c.getString(gpsLatitud), c.getString(gpsLongitud));
        }
        c.close();
        return resultado;
    }

    /**
     * Verifica si un cliente existe
     *
     * @param cliente
     * @return
     */
    public boolean existeCliente(Cliente cliente) {
        boolean resultado = false;
        String[] campos = new String[]{_ID};
        String[] args = new String[]{Integer.toString(cliente.getId())};

        Cursor c = this.getReadableDatabase().query("cliente", campos, _ID + "=?", args, null, null, null);
        int id;

        id = c.getColumnIndex(_ID);

        while (c.moveToNext()) {
            if (c.getInt(id) == cliente.getId())
                resultado = true;
        }

        c.close();
        return resultado;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                       Usuario                                             //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Elimina todos los datos desde la tabla usuarios.
     */
    public void eliminarTablaUsuarios() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("usuarios", null, null);
        db.execSQL("DELETE FROM usuarios");
    }

    /**
     * Insertar usuario.
     *
     * @param id       Identificador de usuario
     * @param email    Email o nombre de usuario
     * @param password Contraseña
     * @param token    Token para comunicacion con servidor
     */
    public void insertarUsuario(int id, String email, String password, String token, int perfil_id) {
        ContentValues valores = new ContentValues();
        Log.d("", valores.toString());
        valores.put(_ID, id);
        valores.put("email", email);
        valores.put("password", password);
        valores.put("token", token);
        valores.put("perfil_id", perfil_id);
        this.getWritableDatabase().insert("usuarios", null, valores);
    }

    /**
     * Actualiza registro de usuario
     *
     * @param usuario Objeto del tipo usuario
     */
    public void actualizarUsuario(Usuario usuario) {
        ContentValues valores = new ContentValues();
        valores.put("password", usuario.getPassword());
        valores.put("token", usuario.getToken());
        valores.put("perfil_id", usuario.getPerfilId());
        this.getWritableDatabase().update("usuarios", valores, _ID + "=" + usuario.getId(), null);
    }

    /**
     * Elimina un registro de usuario.
     *
     * @param email Email o nombre de usuario
     * @return string status
     */
    public String eliminarUsuario(String email) {
        String[] args = new String[]{email};
        this.getReadableDatabase().execSQL("DELETE FROM usuarios WHERE email=?", args);
        return "Registro Eliminado";
    }

    /**
     * Busca registro de usuario.
     *
     * @param email Email o nombre de usuario
     * @return objeto de la clase Usuario
     */
    public Usuario buscarUsuario(String email) {
        Usuario resultado = null;
        String[] campos = new String[]{_ID, "email", "password", "token", "perfil_id"};
        String[] args = new String[]{email};

        Cursor c = this.getReadableDatabase().query("usuarios", campos, "email=?", args, null, null, null);
        int id, mail, pass, token, perfil_id;

        id = c.getColumnIndex(_ID);
        mail = c.getColumnIndex("email");
        pass = c.getColumnIndex("password");
        token = c.getColumnIndex("token");
        perfil_id = c.getColumnIndex("perfil_id");

        while (c.moveToNext()) {
            Log.e(null, "buscarUsuario: ");
            resultado = new Usuario(Integer.parseInt(c.getString(id)),
                    c.getString(mail),
                    c.getString(pass),
                    c.getString(token),
                    Integer.parseInt(c.getString(perfil_id)));
        }

        c.close();
        return resultado;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                            Rutas                                           //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica que un id de ruta exista.
     *
     * @param ruta objeto con todos los datos de la unidad de lectura
     * @return boolean que indica si la ruta existe o no
     */
    public boolean existeRuta(Ruta ruta) {
        boolean resultado = false;
        String[] campos = new String[]{_ID};
        String[] args = new String[]{Integer.toString(ruta.getId())};

        Cursor c = this.getReadableDatabase().query("ruta", campos, _ID + "=?", args, null, null, null);
        int id;

        id = c.getColumnIndex(_ID);

        while (c.moveToNext()) {
            if (c.getInt(id) == ruta.getId())
                resultado = true;
        }

        c.close();
        return resultado;
    }

    /**
     * Busca ruta de acuerdo al ID
     *
     * @param rutaId Id de ruta
     * @return Objeto Ruta con todos los datos del id buscado
     */
    //TODO: Revisar si este metodo aun es necesario.
    public Ruta buscarRuta(String rutaId) {
        Ruta resultado = null;
        String[] campos = new String[]{_ID, "codigo", "nombre", "usuario"};
        String[] args = new String[]{rutaId};

        Cursor c = this.getReadableDatabase().query("ruta", campos, _ID + "=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int nombre = c.getColumnIndex("nombre");
        int usuario = c.getColumnIndex("usuario");

        while (c.moveToNext()) {
            if (c.getString(id).equals(rutaId)) {
                resultado = new Ruta(c.getInt(id), c.getString(codigo), c.getString(nombre), c.getInt(usuario),
                        numeroOrdenesRuta(c.getInt(id)));
            }
        }

        c.close();
        return resultado;
    }

    /**
     * Insertar ruta.
     *
     * @param ruta
     */
    public void insertarRuta(Ruta ruta) {
        if (!this.existeRuta(ruta)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, ruta.getId());
            valores.put("codigo", ruta.getCodigo());
            valores.put("nombre", ruta.getNombre());
            valores.put("usuario", ruta.getUsuario());

            this.getWritableDatabase().insert("ruta", null, valores);
        }
    }

    /**
     * Leer rutas y retornar un arreglo de ellas de acuerdo al usuario logeado.
     *
     * @param usr         Id usuario logeado
     * @param tipoLectura Indica tipo de ordenes a cargar | 1 -> Lectura Normal | 2 -> Lectura Control
     * @return the array list
     */
    public ArrayList<Ruta> leerRutas(int usr, int tipoLectura) {
        ArrayList<Ruta> resultado = new ArrayList<>();

        String filas[] = {_ID, "codigo", "nombre", "usuario"};
        String[] args = new String[]{Integer.toString(usr)};

        Cursor c = this.getReadableDatabase().query("ruta", filas, "usuario=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int nombre = c.getColumnIndex("nombre");
        int usuario = c.getColumnIndex("usuario");

        while (c.moveToNext()) {
            Ruta ruta = new Ruta(c.getInt(id), c.getString(codigo),
                    c.getString(nombre), c.getInt(usuario), leerOrdenes(c.getInt(id), tipoLectura, null));
            //Se muestran solo las rutas con un numero de ordenes pendientes mayor a cero.
            if (ruta.getLecturas().size() != 0)
                resultado.add(ruta);
        }

        c.close();
        return resultado;
    }

    /**
     * Obtiene numero de ordenes de lectura pendientes para una ruta.
     *
     * @param id Id ruta
     * @return cantidad de ordenes pendientes de ruta ingresada
     */
    private int numeroOrdenesRuta(int id) {
        SQLiteStatement statement = this.getReadableDatabase().compileStatement(
                "select count(*) from ordenlectura where ruta_id='" +
                        Integer.toString(id) + "' and estado_lectura_id = '3'");

        return (int) (long) statement.simpleQueryForLong();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                     INSTALACIONES                                          //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta un nuevo registro de instalación
     *
     * @param instalacion
     */
    public void insertarInstalacion(Instalacion instalacion) {
        if (!this.existeInstalacion(instalacion)) {


            ContentValues valores = new ContentValues();
            valores.put(_ID, instalacion.getId());
            valores.put("codigo", instalacion.getCodigo());

            this.getWritableDatabase().insert("instalacion", null, valores);
        }
    }

    /**
     * Verifica si una instalacion ya existe en la base de datos
     *
     * @param instalacion
     * @return
     */
    public boolean existeInstalacion(Instalacion instalacion) {
        boolean resultado = false;
        String[] campos = new String[]{_ID};
        String[] args = new String[]{Integer.toString(instalacion.getId())};

        Cursor c = this.getReadableDatabase().query("instalacion", campos, _ID + "=?", args, null, null, null);
        int id;

        id = c.getColumnIndex(_ID);

        while (c.moveToNext()) {
            if (c.getInt(id) == instalacion.getId())
                resultado = true;
        }

        c.close();
        return resultado;

    }

    /**
     * Busca un registro de instalacion de acuerdo a su ID
     *
     * @param idInstalacion Id instalacion
     * @return Objeto del tipo instalacion correspondiente al id buscado
     */
    public Instalacion buscarInstalacion(int idInstalacion) {
        Instalacion resultado = null;
        String[] campos = new String[]{_ID, "codigo"};
        String[] args = new String[]{Integer.toString(idInstalacion)};

        Cursor c = this.getReadableDatabase().query("instalacion", campos, _ID + "=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");

        while (c.moveToNext()) {
            resultado = new Instalacion(c.getInt(id), c.getString(codigo));
        }

        c.close();
        return resultado;
    }

    /**
     * Busca un Tarifa de acuerdo a su id
     *
     * @param idtarifa id de medidor a buscar
     * @return resultado Objeto de tipo Tarifa
     */
    public Tarifa buscartarifa(int idtarifa) {
        Tarifa resultado = null;
        String[] campos = new String[]{_ID, "nombre"};

        String[] args = new String[]{Integer.toString(idtarifa)};

        Cursor c = this.getReadableDatabase().query("tarifa", campos, _ID + "=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int nombre = c.getColumnIndex("nombre");

        while (c.moveToNext()) {
            resultado = new Tarifa(c.getInt(id), c.getString(nombre));
        }

        c.close();
        return resultado;
    }

    /**
     * Verifica existencia de un factor en la base de datos
     *
     * @param factor objeto de tipo tarifa
     * @return boolean
     */
    public boolean existeFactor(Tarifa factor) {
        boolean resultado = false;
        String[] campos = new String[]{_ID};
        String[] args = new String[]{Integer.toString(factor.getId())};

        Cursor c = this.getReadableDatabase().query("tarifa", campos, _ID + "=?", args, null, null, null);
        int id;

        id = c.getColumnIndex(_ID);

        while (c.moveToNext()) {
            if (c.getInt(id) == factor.getId())
                resultado = true;
        }

        c.close();
        return resultado;
    }


    /**
     * Inserta un nuevo registro en la tabla de medidores.
     *
     * @param factor
     */
    public void insertartarifa(Tarifa factor) {
        if (!this.existeFactor(factor)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, factor.getId());
            valores.put("nombre", factor.getNombre());

            this.getWritableDatabase().insert("tarifa", null, valores);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                         MEDIDOR                                            //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta un nuevo registro en la tabla de medidores.
     *
     * @param medidor
     */
    public void insertarMedidor(Medidor medidor) {
        if (!this.existeMedidor(medidor)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, medidor.getId());
            valores.put("numero_medidor", medidor.getNumeroMedidor());
            valores.put("modelo_medidor_id", medidor.getModeloMedidorId());
            valores.put("ubicacion_medidor",medidor.getUbicacionMedidor());
            valores.put("numero_digitos", medidor.getNumeroDigitos());
            this.getWritableDatabase().insert("medidor", null, valores);
        }
    }

    /**
     * Busca un medidor de acuerdo a su id
     *
     * @param idMedidor id de medidor a buscar
     * @return resultado Objeto de tipo medidor
     */
    public Medidor buscarMedidor(int idMedidor) {
        Medidor resultado = null;
        String[] campos = new String[]{_ID, "numero_medidor", "numero_digitos","ubicacion_medidor","modelo_medidor_id", "diametro", "ano"};

        String[] args = new String[]{Integer.toString(idMedidor)};

        Cursor c = this.getReadableDatabase().query("medidor", campos, _ID + "=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int numeroMedidor = c.getColumnIndex("numero_medidor");
        int numeroDigitos = c.getColumnIndex("numero_digitos");
        int ubicacionMedidor = c.getColumnIndex("ubicacion_medidor");
        int modeloMedidorId = c.getColumnIndex("modelo_medidor_id");

        while (c.moveToNext()) {
            resultado = new Medidor(
                    c.getInt(id),
                    c.getString(numeroMedidor),
                    c.getString(numeroDigitos),
                    c.getString(ubicacionMedidor),
                    c.getInt(modeloMedidorId));
        }
        c.close();
        return resultado;
    }

    /**
     * Verifica existencia de un medidor en la base de datos
     *
     * @param medidor objeto de tipo medidor
     * @return boolean
     */
    public boolean existeMedidor(Medidor medidor) {
        boolean resultado = false;
        String[] campos = new String[]{_ID};
        String[] args = new String[]{Integer.toString(medidor.getId())};

        Cursor c = this.getReadableDatabase().query("medidor", campos, _ID + "=?", args, null, null, null);
        int id;

        id = c.getColumnIndex(_ID);

        while (c.moveToNext()) {
            if (c.getInt(id) == medidor.getId())
                resultado = true;
        }

        c.close();
        return resultado;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                  ORDENES DE LECTURA                                        //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Insertar ordenlectura.
     *
     * @param orden
     */
    public boolean insertarOrdenlectura(OrdenLectura orden) {
        Log.d("Insertar: ", orden.getCodigo());
        if (!this.existeOrdenLectura(orden.getId())) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, orden.getId());
            valores.put("codigo", orden.getCodigo());
            valores.put("secuencia_lector", orden.getSecuencia_lector());
            valores.put("direccion", orden.getDireccion());
            valores.put("gps_latitud", orden.getGpsLatitud());
            valores.put("gps_longitud", orden.getGpsLongitud());
            valores.put("instalacion_id", orden.getInstalacionId());
            valores.put("cliente_id", orden.getClienteId());
            valores.put("ruta_id", orden.getRutaId());
            valores.put("tipo_lectura_id", orden.getTipoLecturaId());
            valores.put("estado_lectura_id", orden.getEstadoLecturaId());
            valores.put("flag_envio", 0);
            valores.put("color", orden.getColor());
            valores.put("medidor_id", orden.getMedidorId());
            valores.put("fecha_ejecucion", orden.getFechaEjecucion());
            valores.put("lectura_anterior", orden.getLecturaAnterior());
            valores.put("consumo_inferior", orden.getConsumoInferior());
            valores.put("consumo_superior", orden.getConsumoSuperior());
            valores.put("numero_medidor", orden.getNumeroMedidor());
            if (this.getWritableDatabase().insert("ordenlectura", null, valores) != -1) {
                return true;
            }
        }
        return false;
    }


    // filasOrden = Lista Publica para acceder a los datos de la orden en todos los metodos
    String filasOrden[] = {_ID,
            "codigo",
            "secuencia_lector",
            "direccion",
            "numero_medidor",
            "instalacion_id",
            "medidor_id",
            "cliente_id",
            "ruta_id",
            "tipo_lectura_id",
            "estado_lectura_id",
            "color",
            "flag_envio",
            "lectura_anterior",
            "consumo_inferior",
            "consumo_superior"};
    /**
     * Leer ordenes.
     *
     * @param rutaCarga   Ruta de la cual se quieren obtener las ordenes de lectura
     * @param tipoLectura Indica tipo de ordenes a cargar | 1 -> Lectura Normal | 2 -> Lectura Verificación
     * @return arreglo con todas las ordenes de la ruta
     * 21/10/2018 > Agrego Color y Numero de digitos
     */
    public ArrayList<OrdenLectura> leerOrdenes(int rutaCarga, int tipoLectura, String orderBy) {
        ArrayList<OrdenLectura> resultado = new ArrayList<>();
        String[] args = new String[]{Integer.toString(rutaCarga), Integer.toString(tipoLectura)};

        Cursor c = this.getReadableDatabase().query("ordenlectura", filasOrden, "ruta_id=? and tipo_lectura_id=?", args, null, null, orderBy);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumo_maximo = c.getColumnIndex("consumo_superior");
        int consumo_minimo = c.getColumnIndex("consumo_inferior");
        int numeroMedidor = c.getColumnIndex("numero_medidor");

        while (c.moveToNext()) {
            if (c.getString(estadoLecturaId).equals("3")) {
                OrdenLectura orden = new OrdenLectura(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getInt(secuencia_lector),
                        c.getString(direccion),
                        c.getString(numeroMedidor),
                        c.getInt(instalacionId),
                        c.getInt (medidor_id),
                        c.getInt(clienteId),
                        c.getInt(rutaId),
                        c.getInt(tipoLecturaId),
                        c.getInt(estadoLecturaId),
                        c.getInt (color),
                        c.getInt(flagEnvio),
                        c.getDouble(lecturaAnterior),
                        c.getDouble(consumo_minimo),
                        c.getDouble(consumo_maximo));
                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }
            }
        }

        c.close();
        return resultado;
    }
    String filas[] = {_ID,
            "codigo",
            "secuencia_lector",
            "direccion",
            "instalacion_id",
            "medidor_id",
            "cliente_id",
            "ruta_id",
            "tipo_lectura_id",
            "estado_lectura_id",
            "color",
            "flag_envio",
            "observacion",
            "tarifa_id",
            "consumo_promedio",
            "lectura_anterior",
            "consumo_inferior",
            "consumo_superior",
            "lectura_actual",
            "consumo_actual",
            "fecha_ejecucion",
            "clave_lectura_id",
            "gps_latitud",
            "gps_longitud",
            "lectura_anterior",
            "numero_medidor"};
    /**
     * Retorna un arreglo con todas las ordenes de lectura pendientes
     *
     * @return Arreglo con todas las ordenes de lectura pendientes
     */
    public ArrayList<OrdenLectura> leerTodasOrdenesPendientes() {
        ArrayList<OrdenLectura> resultado = new ArrayList<>();

        //Parametro de ordenes pendientes estado_lectura_id = 3.
        String[] args = new String[]{Integer.toString(3)};

        Cursor c = this.getReadableDatabase().query("ordenlectura", filas, "estado_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int tarifaId = c.getColumnIndex("tarifa_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int observacion =c.getColumnIndex("observacion");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int consumoActual = c.getColumnIndex("consumo_actual");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int numeroMedidor = c.getColumnIndex("numero_medidor");

        while (c.moveToNext()) {
                OrdenLectura orden = new OrdenLectura(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getInt(secuencia_lector),
                        c.getString(direccion),
                        c.getString(numeroMedidor),
                        c.getDouble(gpsLatitud),
                        c.getDouble(gpsLongitud),
                        c.getInt(instalacionId),
                        c.getInt (medidor_id),
                        c.getInt(clienteId),
                        c.getInt(rutaId),
                        c.getInt(tipoLecturaId),
                        c.getInt(estadoLecturaId),
                        c.getInt(tarifaId),
                        c.getInt (color),
                        c.getInt(flagEnvio),
                        c.getString(observacion),
                        c.getDouble(lecturaAnterior),
                        c.getDouble(consumoPromedio),
                        c.getDouble(lecturaActual),
                        c.getInt(consumoActual),
                        c.getDouble(consumoSuperior),
                        c.getDouble(consumoInferior),
                        c.getLong(fechaEjecucion),
                        c.getInt(claveLecturaId),
                        this.verFotografias(c.getInt(id)));
                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }

        }
        c.close();
        return resultado;
    }


    /**
     * Retorna un arreglo con una lista de ordenes que aun no se actualizan en el servidor.
     *
     * @return Arreglo con una lista de ordenes que aun no se actualizan en el servidor.
     */
    public ArrayList<OrdenLectura> listaOrdenesSinEnviar() {
        ArrayList<OrdenLectura> resultado = new ArrayList<>();

        //Ordenes leidas sin enviar deben estar en estado_lectura_id = 4.
        String[] args = new String[]{"4"};

        Cursor c = this.getReadableDatabase().query("ordenlectura", filas, "estado_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int tarifaId = c.getColumnIndex("tarifa_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int observacion =c.getColumnIndex("observacion");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int consumoActual = c.getColumnIndex("consumo_actual");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int numeroMedidor = c.getColumnIndex("numero_medidor");

        while (c.moveToNext()) {
            if (c.getString(flagEnvio).equals("0")) {
                OrdenLectura orden = new OrdenLectura(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getInt(secuencia_lector),
                        c.getString(direccion),
                        c.getString(numeroMedidor),
                        c.getDouble(gpsLatitud),
                        c.getDouble(gpsLongitud),
                        c.getInt(instalacionId),
                        c.getInt (medidor_id),
                        c.getInt(clienteId),
                        c.getInt(rutaId),
                        c.getInt(tipoLecturaId),
                        c.getInt(estadoLecturaId),
                        c.getInt(tarifaId),
                        c.getInt (color),
                        c.getInt(flagEnvio),
                        c.getString(observacion),
                        c.getDouble(lecturaAnterior),
                        c.getDouble(consumoPromedio),
                        c.getDouble(lecturaActual),
                        c.getInt(consumoActual),
                        c.getDouble(consumoSuperior),
                        c.getDouble(consumoInferior),
                        c.getLong(fechaEjecucion),
                        c.getInt(claveLecturaId),
                        this.verFotografias(c.getInt(id)));
                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }
            }
        }

        c.close();
        return resultado;
    }

    /**
     * Retorna un arreglo con una lista de ordenes que aun no se ha ingresado lectura.
     *
     * @return Arreglo con una lista de ordenes que aun no se actualizan en el servidor.
     */
    public ArrayList<OrdenLectura> listaOrdenesPendientes() {
        ArrayList<OrdenLectura> resultado = new ArrayList<>();
        //Ordenes sin lectura deben estar en estado_lectura_id = 3.
        String[] args = new String[]{"3"};

        Cursor c = this.getReadableDatabase().query("ordenlectura", filasOrden, "estado_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumo_maximo = c.getColumnIndex("consumo_superior");
        int consumo_minimo = c.getColumnIndex("consumo_inferior");
        int numeroMedidor = c.getColumnIndex("numero_medidor");

        while (c.moveToNext()) {
            if (c.getString(flagEnvio).equals("0")) {
                OrdenLectura orden = new OrdenLectura(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getInt(secuencia_lector),
                        c.getString(direccion),
                        c.getString(numeroMedidor),
                        c.getInt(instalacionId),
                        c.getInt (medidor_id),
                        c.getInt(clienteId),
                        c.getInt(rutaId),
                        c.getInt(tipoLecturaId),
                        c.getInt(estadoLecturaId),
                        c.getInt (color),
                        c.getInt(flagEnvio),
                        c.getDouble(lecturaAnterior),
                        c.getDouble(consumo_minimo),
                        c.getDouble(consumo_maximo));
                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }
            }
        }
        c.close();
        return resultado;
    }

    /**
     * Verifica si es que existe una orden de acuerdo a su id.
     *
     * @param ordenId id de orden de lectura
     * @return boolean que indica si la orden existe dentro de la BD
     */
    public boolean existeOrdenLectura(int ordenId) {
        String filas[] = {_ID};
        String[] args = new String[]{Integer.toString(ordenId)};

        Cursor c = this.getReadableDatabase().query("ordenlectura", filas, _ID + "=?", args, null, null, null);
        if (c.moveToNext())
            return true;

        c.close();
        return false;
    }

    /**
     * Actualizar orden.
     *
     * @param orden Objeto de tipo OrdenLectura.
     */
    public void actualizarOrden(OrdenLectura orden) {
        ContentValues valores = new ContentValues();
        valores.put("estado_lectura_id", orden.getEstadoLecturaId());
        valores.put("gps_latitud", orden.getGpsLatitud());
        valores.put("gps_longitud", orden.getGpsLongitud());
        valores.put("observacion", orden.getObservacion());
        valores.put("fecha_ejecucion", orden.getFechaEjecucion());
        valores.put("lectura_actual", orden.getLecturaActual());
        valores.put("consumo_actual", orden.getConsumoActual());
        valores.put("clave_lectura_id", orden.getClaveLecturaId());
        this.getWritableDatabase().update("ordenlectura", valores, _ID + "=" + orden.getId(), null);


    }

    /**
     * Elimina una orden de lectura dado un id
     *
     * @param id id de orden a eliminar
     */
    public void eliminarOrden(int id) {
        this.getReadableDatabase().execSQL("DELETE FROM ordenlectura WHERE " + _ID + "=" + Integer.toString(id));
    }

    /**
     * Buscar ordenes por numero de medidor
     * modo busqueda -> 1 = busqueda de ordenes en estado 3
     * -> 2 = busqueda de ordenes en estado 4 y facturadas
     * -> 3 = busqueda de ordenes en estado 4 autorizadas a facturar
     */
    public ArrayList<OrdenLectura> listarOrdenesMedidor(String medidor, int modoBusqueda, String orderBy) {
        ArrayList<OrdenLectura> listaOrdenes = new ArrayList<>();

        String[] args = new String[]{"%" + medidor + "%"};
        String query = "";
        switch (modoBusqueda) {
            case 1:
                query = "SELECT * FROM ordenlectura WHERE numero_medidor LIKE ? and ordenlectura.estado_lectura_id = 3 " + "ORDER BY " + orderBy;
                break;
            case 2:
                query = "SELECT * FROM ordenlectura WHERE numero_medidor LIKE ? and ordenlectura.facturado=1 " + "ORDER BY " + orderBy;
                break;
            case 3:
                query = "SELECT * FROM ordenlectura WHERE numero_medidor LIKE ? and ordenlectura.autorizado_facturacion = 1 and ordenlectura.facturado=0 " + "ORDER BY " + orderBy;
                break;
        }

        Cursor c = this.getReadableDatabase().rawQuery(query, args);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int tarifaId = c.getColumnIndex("tarifa_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int observacion =c.getColumnIndex("observacion");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int consumoActual = c.getColumnIndex("consumo_actual");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int numeroMedidor = c.getColumnIndex("numero_medidor");

        while (c.moveToNext()) {
            listaOrdenes.add(new OrdenLectura(
                    c.getInt(id),
                    c.getString (codigo),
                    c.getInt(secuencia_lector),
                    c.getString(direccion),
                    c.getString(numeroMedidor),
                    c.getDouble(gpsLatitud),
                    c.getDouble(gpsLongitud),
                    c.getInt(instalacionId),
                    c.getInt (medidor_id),
                    c.getInt(clienteId),
                    c.getInt(rutaId),
                    c.getInt(tipoLecturaId),
                    c.getInt(estadoLecturaId),
                    c.getInt(tarifaId),
                    c.getInt (color),
                    c.getInt(flagEnvio),
                    c.getString(observacion),
                    c.getDouble(lecturaAnterior),
                    c.getDouble(consumoPromedio),
                    c.getDouble(lecturaActual),
                    c.getInt(consumoActual),
                    c.getDouble(consumoSuperior),
                    c.getDouble(consumoInferior),
                    c.getLong(fechaEjecucion),
                    c.getInt(claveLecturaId)));
        }

        c.close();
        return listaOrdenes;

    }


    /**
     * Buscar ordenes por numero de medidor
     * modo busqueda -> 1 = busqueda de ordenes en estado 3
     * -> 2 = busqueda de ordenes en estado 4 y facturadas
     * -> 3 = busqueda de ordenes en estado 4 autorizadas a facturar
     */
    public ArrayList<OrdenLectura> listarOrdenesSecuencia(String medidor, int modoBusqueda, String orderBy) {
        ArrayList<OrdenLectura> listaOrdenes = new ArrayList<>();

        String[] args = new String[]{"%" + medidor + "%"};
        String query = "";
        switch (modoBusqueda) {
            case 1:
                query = "SELECT * FROM ordenlectura INNER JOIN medidor ON ordenlectura.medidor_id = medidor." +
                        _ID + " WHERE medidor.numero_medidor LIKE ? and ordenlectura.estado_lectura_id = 3 " + "ORDER BY " + orderBy;
                break;
            case 2:
                query = "SELECT * FROM ordenlectura INNER JOIN medidor ON ordenlectura.medidor_id = medidor." +
                        _ID + " WHERE medidor.numero_medidor LIKE ? and ordenlectura.facturado=1" + "ORDER BY " + orderBy;
                break;
            case 3:
                query = "SELECT * FROM ordenlectura INNER JOIN medidor ON ordenlectura.medidor_id = medidor." +
                        _ID + " WHERE medidor.numero_medidor LIKE ? and ordenlectura.autorizado_facturacion = 1 and ordenlectura.facturado=0" + "ORDER BY " + orderBy;
                break;
        }

        Cursor c = this.getReadableDatabase().rawQuery(query, args);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int tarifaId = c.getColumnIndex("tarifa_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int observacion =c.getColumnIndex("observacion");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int consumoActual = c.getColumnIndex("consumo_actual");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int numeroMedidor = c.getColumnIndex("numero_medidor");


        while (c.moveToNext()) {
            listaOrdenes.add(new OrdenLectura(
                    c.getInt(id),
                    c.getString (codigo),
                    c.getInt(secuencia_lector),
                    c.getString(direccion),
                    c.getString(numeroMedidor),
                    c.getDouble(gpsLatitud),
                    c.getDouble(gpsLongitud),
                    c.getInt(instalacionId),
                    c.getInt (medidor_id),
                    c.getInt(clienteId),
                    c.getInt(rutaId),
                    c.getInt(tipoLecturaId),
                    c.getInt(estadoLecturaId),
                    c.getInt(tarifaId),
                    c.getInt (color),
                    c.getInt(flagEnvio),
                    c.getString(observacion),
                    c.getDouble(lecturaAnterior),
                    c.getDouble(consumoPromedio),
                    c.getDouble(lecturaActual),
                    c.getInt(consumoActual),
                    c.getDouble(consumoSuperior),
                    c.getDouble(consumoInferior),
                    c.getLong(fechaEjecucion),
                    c.getInt(claveLecturaId)));
        }

        c.close();
        return listaOrdenes;

    }

    /**
     * Busca ordenes de lectura por la direccion de la orden
     * @param direccionBusqueda Direccion busqueda
     * @return Arreglo con todas las ordenes que contengan direcciones similares a lo buscado
     */
    public ArrayList<OrdenLectura> listarOrdenesDireccion(String direccionBusqueda, int modoBusqueda, String orderBy) {
        ArrayList<OrdenLectura> listaOrdenes = new ArrayList<>();

        String[] args = new String[]{"%" + direccionBusqueda + "%"};
        String query = "";
        switch (modoBusqueda) {
            case 1:
                query = "SELECT * FROM ordenlectura WHERE direccion LIKE ? and ordenlectura.estado_lectura_id = 3 " + "ORDER BY " + orderBy;
                break;
            case 2:
                query = "SELECT * FROM ordenlectura WHERE direccion LIKE ? and ordenlectura.facturado=1 " + "ORDER BY " + orderBy;
                break;
            case 3:
                query = "SELECT * FROM ordenlectura WHERE direccion LIKE ? and ordenlectura.autorizado_facturacion = 1 and ordenlectura.facturado=0 " + "ORDER BY " + orderBy;
                break;

        }

        Cursor c = this.getReadableDatabase().rawQuery(query, args);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int secuencia_lector = c.getColumnIndex("secuencia_lector");
        int direccion = c.getColumnIndex("direccion");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidor_id = c.getColumnIndex("medidor_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int rutaId = c.getColumnIndex("ruta_id");
        int tipoLecturaId = c.getColumnIndex("tipo_lectura_id");
        int estadoLecturaId = c.getColumnIndex("estado_lectura_id");
        int tarifaId = c.getColumnIndex("tarifa_id");
        int color = c.getColumnIndex("color");
        int flagEnvio = c.getColumnIndex("flag_envio");
        int observacion =c.getColumnIndex("observacion");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int consumoActual = c.getColumnIndex("consumo_actual");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int numeroMedidor = c.getColumnIndex("numero_medidor");


        while (c.moveToNext()) {
            listaOrdenes.add(new OrdenLectura(
                    c.getInt(id),
                    c.getString (codigo),
                    c.getInt(secuencia_lector),
                    c.getString(direccion),
                    c.getString(numeroMedidor),
                    c.getDouble(gpsLatitud),
                    c.getDouble(gpsLongitud),
                    c.getInt(instalacionId),
                    c.getInt (medidor_id),
                    c.getInt(clienteId),
                    c.getInt(rutaId),
                    c.getInt(tipoLecturaId),
                    c.getInt(estadoLecturaId),
                    c.getInt(tarifaId),
                    c.getInt (color),
                    c.getInt(flagEnvio),
                    c.getString(observacion),
                    c.getDouble(lecturaAnterior),
                    c.getDouble(consumoPromedio),
                    c.getDouble(lecturaActual),
                    c.getInt(consumoActual),
                    c.getDouble(consumoSuperior),
                    c.getDouble(consumoInferior),
                    c.getLong(fechaEjecucion),
                    c.getInt(claveLecturaId)));
        }

        c.close();
        return listaOrdenes;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                               DETALLE ORDENE DE LECTURA                                    //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta un detalle de orden en la base de datos.
     *
     * @param detalle
     */
    public void insertarDetalleOrdenlectura(DetalleOrdenLectura detalle) {
        if (!this.existeDetalleOrdenLectura(detalle)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, detalle.getId());
            valores.put("orden_lectura_id", detalle.getOrdenLecturaId());
            valores.put("lectura_anterior", detalle.getLecturaAnterior());
            valores.put("consumo_promedio", detalle.getConsumoPromedio());
            valores.put("lectura_actual", detalle.getLecturaActual());
            valores.put("consumo_superior", detalle.getConsumoSuperior());
            valores.put("consumo_inferior", detalle.getConsumoInferior());
            valores.put("fecha_ejecucion", detalle.getFechaEjecucion());
            valores.put("clave_lectura_id", detalle.getClaveLecturaId());
            valores.put("consumo_actual", detalle.getConsumoActual());
            this.getWritableDatabase().insert("detalleordenlectura", null, valores);
        }
    }

    /**
     * Verifica la existencia de un detalle de orden de lectura en la base de datos.
     *
     * @param detalle objeto de tipo detalle
     * @return boolean
     */
    public boolean existeDetalleOrdenLectura(DetalleOrdenLectura detalle) {
        String filas[] = {_ID};
        String[] args = new String[]{Integer.toString(detalle.getId())};

        Cursor c = this.getReadableDatabase().query("detalleordenlectura", filas, _ID + "=?", args, null, null, null);
        if (c.moveToNext())
            return true;

        c.close();
        return false;
    }

    /**
     * Retorna todos los detalles(Numeradores) de una orden de lectura
     *
     * @param ordenId id de orden de la que se quieren obtener los detalles.
     * @return
     */
    public ArrayList<DetalleOrdenLectura> leerDetalleOrdenLectura(int ordenId) {
        ArrayList<DetalleOrdenLectura> resultado = new ArrayList<>();

        String filas[] = {_ID, "orden_lectura_id", "lectura_anterior", "consumo_promedio",
                "lectura_actual", "consumo_superior", "consumo_inferior", "fecha_ejecucion",
                "clave_lectura_id", "consumo_actual"};

        String[] args = new String[]{Integer.toString(ordenId)};

        Cursor c = this.getReadableDatabase().query("detalleordenlectura", filas, "orden_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int ordenLecturaId = c.getColumnIndex("orden_lectura_id");
        int lecturaAnterior = c.getColumnIndex("lectura_anterior");
        int consumoPromedio = c.getColumnIndex("consumo_promedio");
        int lecturaActual = c.getColumnIndex("lectura_actual");
        int consumoSuperior = c.getColumnIndex("consumo_superior");
        int consumoInferior = c.getColumnIndex("consumo_inferior");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int claveLecturaId = c.getColumnIndex("clave_lectura_id");
        int consumoActual = c.getColumnIndex("consumo_actual");

        while (c.moveToNext()) {
            resultado.add(new DetalleOrdenLectura(c.getInt(id),
                    c.getInt(ordenLecturaId),
                    c.getDouble(lecturaAnterior),
                    c.getDouble(consumoPromedio),
                    c.getDouble(lecturaActual),
                    c.getDouble(consumoSuperior),
                    c.getDouble(consumoInferior),
                    c.getLong(fechaEjecucion),
                    c.getInt(claveLecturaId),
                    c.getInt(consumoActual),
                    this.verIntentos(c.getInt(id)),
                    this.verFotografias(c.getInt(id))));
        }

        c.close();
        return resultado;
    }

    /**
     * Elimina detalle dado un id
     *
     * @param id id de detalle a eliminar
     */
    public void eliminarDetalles(int id) {
        this.getReadableDatabase().execSQL("DELETE FROM detalleordenlectura WHERE orden_lectura_id=" + Integer.toString(id));
    }

    /**
     * Actualiza un registro de detalle de lectura.
     *
     * @param detalle Objeto del tipo DetalleOrdenLectura
     */
    public void actualizarDetalleOrden(OrdenLectura detalle) {
        ContentValues valores = new ContentValues();
        valores.put("lectura_actual", detalle.getLecturaActual());
        valores.put("consumo_actual", detalle.getConsumoActual());
        valores.put("fecha_ejecucion", detalle.getFechaEjecucion());
        valores.put("clave_lectura_id", detalle.getClaveLecturaId());
        this.getWritableDatabase().update("detalleordenlectura", valores, _ID + "=" + detalle.getId(), null);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                      CLAVES DE LECTURA                                     //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Insertar clave.
     *
     * @param claveLectura Objeto del tipo ClaveLectura
     */
    public void insertarClave(ClaveLectura claveLectura) {
        ContentValues valores = new ContentValues();
        valores.put(_ID, claveLectura.getId());
        valores.put("nombre", claveLectura.getNombre());
        valores.put("codigo", claveLectura.getCodigo());
        valores.put("numero_fotografias", claveLectura.getNumeroFotografias());
        valores.put("efectivo", claveLectura.getEfectivo());
        valores.put("requerido", claveLectura.getRequerido());
        this.getWritableDatabase().insert("clavelectura", null, valores);
    }

    /**
     * Retorna arreglo con las claves de lectura existentes.
     *
     * @return the array list
     */
    public ArrayList<ClaveLectura> leerClaves() {
        ArrayList<ClaveLectura> resultado = new ArrayList<>();

        String filas[] = {_ID, "clave", "codigo","nombre", "numero_fotografias", "requerido", "efectivo","factura"};

        Cursor c = this.getReadableDatabase().query("clavelectura", filas, null, null, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int nombre = c.getColumnIndex("nombre");
        int numeroFotografias = c.getColumnIndex("numero_fotografias");
        int requerido = c.getColumnIndex("requerido");
        int efectivo = c.getColumnIndex("efectivo");
        int factura = c.getColumnIndex("factura");

        boolean clvRequerido = false;
        boolean clvEfectivo = false;
        boolean clvFactura = false;

        while (c.moveToNext()) {
            if (c.getInt(requerido) == 1)
                clvRequerido = true;
            if (c.getInt(efectivo) == 1)
                clvEfectivo = true;
            if (c.getInt(factura) == 1)
                clvEfectivo = true;
            //if (habilitado == true) {
                resultado.add(new ClaveLectura(
                        c.getInt(id),
                        c.getString(codigo),
                        c.getString(nombre),
                        c.getInt(numeroFotografias),
                        clvRequerido,
                        clvEfectivo,
                        clvFactura));
            //}
            //requerida = false;
        }

        c.close();
        return resultado;
    }

    /**
     * Eliminar claves lectura.
     */
    public void eliminarClavesLectura() {
        this.getReadableDatabase().execSQL("DELETE FROM clavelectura");
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                  PARAMETROS SERVIDOR                                       //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Insertar parametros.
     *
     * @param parametros the parametros
     */
    public void insertarParametros(ParametrosServidor parametros) {
        ContentValues valores = new ContentValues();
        valores.put(_ID, parametros.getId());
        valores.put("ip", parametros.getIp());
        valores.put("intervalo", parametros.getIntervalo());
        this.getWritableDatabase().insert("parametros", null, valores);
    }

    /**
     * Actualizar parametros.
     *
     * @param parametros the parametros
     */
    public void actualizarParametros(ParametrosServidor parametros) {
        ContentValues valores = new ContentValues();
        valores.put("ip", parametros.getIp());
        valores.put("intervalo", parametros.getIntervalo());

        this.getWritableDatabase().update("parametros", valores, _ID + "=" + parametros.getId(), null);
    }

    /**
     * Buscar parametros parametros servidor.
     *
     * @param idBusqueda the id busqueda
     * @return the parametros servidor
     */
    public ParametrosServidor buscarParametros(int idBusqueda) {
        ParametrosServidor resultado = null;
        String[] campos = new String[]{_ID, "ip", "intervalo"};

        Cursor c = this.getReadableDatabase().query("parametros", campos, _ID + "=" + idBusqueda, null, null, null, null);
        int id, ip, intervalo;

        id = c.getColumnIndex(_ID);
        ip = c.getColumnIndex("ip");
        intervalo = c.getColumnIndex("intervalo");

        while (c.moveToNext()) {
            resultado = new ParametrosServidor(Integer.parseInt(c.getString(id)), c.getString(ip), Integer.parseInt(c.getString(intervalo)));
        }

        c.close();

        return resultado;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                   PERFILES DE USUARIO                                      //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Insertar perfil.
     *
     * @param id     the id
     * @param nombre the nombre
     * @param imagen the imagen
     */
    public void insertarPerfil(int id, String nombre, String imagen) {
        ContentValues valores = new ContentValues();
        valores.put(_ID, id);
        valores.put("nombre", nombre);
        valores.put("imagen", imagen);
        this.getWritableDatabase().insert("perfiles", null, valores);
    }

    /**
     * Elimina todos los perfiles en la base de datos.
     */
    public void eliminarPerfiles() {
        this.getReadableDatabase().execSQL("DELETE FROM perfiles");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                         DATOS EQUIPO                                       //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Insertar equipo.
     *
     * @param id     the id
     * @param nombre the nombre
     * @param mac    the mac
     */
    public void insertarEquipo(int id, String nombre, String mac) {
        ContentValues valores = new ContentValues();
        valores.put(_ID, id);
        valores.put("nombre", nombre);
        valores.put("mac", mac);
        this.getWritableDatabase().insert("equipos", null, valores);
    }

    /**
     * Elimina equipos.
     */
    public void eliminarEquipos() {
        this.getReadableDatabase().execSQL("DELETE FROM equipos");
    }

    /**
     * Busca un equipo en la base de datos de acuerdo a la mac
     *
     * @param macEquipo
     * @return
     */
    public boolean buscarEquipoMac(String macEquipo) {
        String[] campos = new String[]{_ID, "nombre", "mac"};
        String[] args = new String[]{macEquipo};

        Cursor c = this.getReadableDatabase().query("equipos", campos, "mac=?", args, null, null, null);

        int mac = c.getColumnIndex("mac");


        while (c.moveToNext()) {
            if (c.getString(mac).equals(macEquipo)) {
                c.close();
                return true;
            }

        }

        c.close();
        return false;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                           FOTOGRAFIA                                       //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Guarda registro en tabla de fotografias.
     *
     * @param fotografia Objeto del tipo fotografia.
     */
    public void insertarFotografia(Fotografia fotografia) {
        ContentValues valores = new ContentValues();
        valores.put("orden_lectura_id", fotografia.getOrdenLecturaId());
        valores.put("archivo", fotografia.getRuta());
        valores.put("descripcion", fotografia.getObservacion());
        valores.put("flag_envio", fotografia.getFlagEnvio());
        this.getWritableDatabase().insert("fotografia", null, valores);

    }

    /**
     * Actualiza registro en tabla de fotografias.
     *
     * @param fotografia Objeto del tipo fotografia
     */
    public void actualizarFotografia(Fotografia fotografia) {
        ContentValues valores = new ContentValues();
        valores.put("orden_lectura_id", fotografia.getOrdenLecturaId());
        valores.put("archivo", fotografia.getRuta());
        valores.put("descripcion", fotografia.getObservacion());
        valores.put("flag_envio", fotografia.getFlagEnvio());
        Log.d("Valores Fotos", valores.toString());
        this.getWritableDatabase().update("fotografia", valores, _ID + "=" + fotografia.getId(), null);
    }

    /**
     * Retorna todas las fotografias correspondientes a un detalle de lectura.
     *
     * @param idDetalle Id Detalle orden
     * @return Retorna arreglo de fotografias correspondiente a un Detalle de orden de lectura
     */
    private ArrayList<Fotografia> verFotografias(int idDetalle) {
        ArrayList<Fotografia> resultado = new ArrayList<>();

        String campos[] = {_ID, "orden_lectura_id", "archivo", "descripcion", "flag_envio"};
        String[] args = new String[]{Integer.toString(idDetalle)};

        Cursor c = this.getReadableDatabase().query("fotografia", campos, "orden_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int ordenLecturaId = c.getColumnIndex("orden_lectura_id");
        int archivo = c.getColumnIndex("archivo");
        int descripcion = c.getColumnIndex("descripcion");
        int flagEnvio = c.getColumnIndex("flag_envio");

        while (c.moveToNext()) {
            resultado.add(new Fotografia(c.getInt(id), c.getInt(ordenLecturaId), c.getString(archivo),
                    c.getString(descripcion), c.getInt(flagEnvio)));
        }

        c.close();
        return resultado;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                                             INTENTOS                                       //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta registro en tabla intentos.
     *
     * @param intento Objeto del tipo Intento.
     */
    public void insertarIntento(Intento intento) {
        ContentValues valores = new ContentValues();
        valores.put("detalle_orden_lectura_id", intento.getIdDetalleOrden());
        valores.put("lectura", intento.getLectura());
        valores.put("flag_envio", intento.getFlagEnvio());
        this.getWritableDatabase().insert("intento", null, valores);

    }

    /**
     * Actualiza registro de intento.
     *
     * @param intento Objeto del tipo intento.
     */
    public void actualizarIntento(Intento intento) {
        ContentValues valores = new ContentValues();
        valores.put("lectura", intento.getLectura());
        valores.put("flag_envio", intento.getFlagEnvio());
        this.getWritableDatabase().update("intento", valores, _ID + "=" + intento.getId(), null);
    }

    /**
     * Retorna arreglo con intentos para un detalle de orden de lectura.
     *
     * @param idDetalleOrdenLectura Id detalle orden de lectura
     * @return Arreglo con intentos ingresados para ese detalle de orden.
     */
    private ArrayList<Intento> verIntentos(int idDetalleOrdenLectura) {
        ArrayList<Intento> resultado = new ArrayList<>();

        String campos[] = {_ID, "detalle_orden_lectura_id", "lectura", "flag_envio"};
        String[] args = new String[]{Integer.toString(idDetalleOrdenLectura)};

        Cursor c = this.getReadableDatabase().query("intento", campos, "detalle_orden_lectura_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int detalleOrdenLecturaId = c.getColumnIndex("detalle_orden_lectura_id");
        int lectura = c.getColumnIndex("lectura");
        int flagEnvio = c.getColumnIndex("flag_envio");

        while (c.moveToNext()) {
            resultado.add(new Intento(c.getInt(id), c.getInt(detalleOrdenLecturaId), c.getDouble(lectura), c.getInt(flagEnvio)));
        }

        c.close();
        return resultado;
    }

    /**
     * Elimina intentos de un numerador.
     *
     * @param id id de numerador al cual se le eliminaran los intentos
     */
    public void eliminarIntentos(int id) {
        this.getReadableDatabase().execSQL("DELETE FROM intento WHERE detalle_orden_lectura_id=" + Integer.toString(id));
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registra los parametros de la impresora
     *
     * @param parametros
     */
    public void insertarParametrosImpresora(ParametrosImpresora parametros) {
        ContentValues valores = new ContentValues();
        valores.put("mac", parametros.getMac());
        valores.put("flag_facturacion", parametros.getFlagImpresion());
        this.getWritableDatabase().insert("parametrosimpresora", null, valores);

    }

    /**
     * Actualiza los parametros de la impresora
     *
     * @param parametros
     */
    public void actualizarParametrosImpresora(ParametrosImpresora parametros) {
        ContentValues valores = new ContentValues();
        valores.put("mac", parametros.getMac());
        valores.put("flag_facturacion", parametros.getFlagImpresion());

        this.getWritableDatabase().update("parametrosimpresora", valores, _ID + "=" + parametros.getId(), null);
    }

    /**
     * Retorna parametros de impresora dado un id
     *
     * @param idBusqueda id a buscar(El valor siempre debe ser 1)
     * @return objeto de tipo Parametros Impresora
     */
    public ParametrosImpresora buscarParametrosImpresora(int idBusqueda) {
        ParametrosImpresora resultado = null;
        String[] campos = new String[]{_ID, "mac", "flag_facturacion"};

        Cursor c = this.getReadableDatabase().query("parametrosimpresora", campos, _ID + "=" + idBusqueda, null, null, null, null);

        int id = c.getColumnIndex(_ID);
        int mac = c.getColumnIndex("mac");
        int flagImpresion = c.getColumnIndex("flag_facturacion");

        while (c.moveToNext()) {
            if (c.getInt(flagImpresion) == 1)
                resultado = new ParametrosImpresora(c.getInt(id), c.getString(mac), true);

            else
                resultado = new ParametrosImpresora(c.getInt(id), c.getString(mac), false);
        }

        c.close();

        return resultado;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                            //
    //                           TABLAS DE ORDENES DE REPARTO                                     //
    //                                                                                            //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserta una ruta de reparto a la base de datos
     * @param ruta
     */

    public void insertarRutaReparto(RutaReparto ruta)
    {
        if(!this.existeRutaReparto(ruta)) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, ruta.getId());
            valores.put("codigo", ruta.getCodigo());
            valores.put("nombre", ruta.getNombre());
            valores.put("usuario", ruta.getUsuario());

            this.getWritableDatabase().insert("rutareparto", null, valores);
        }
    }
    /**
     * Devuelve Array con rutas de reparto
     * @param usr > usuario
     */
    public ArrayList<RutaReparto> leerRutasReparto(int usr)
    {
        ArrayList<RutaReparto> resultado = new ArrayList<>();

        String filas[] = {_ID, "codigo", "nombre", "usuario"};
        String[] args = new String[] {Integer.toString(usr)};

        Cursor c = this.getReadableDatabase().query("rutareparto", filas,"usuario=?", args, null, null, null);
        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int nombre = c.getColumnIndex("nombre");
        int usuario = c.getColumnIndex("usuario");
        while(c.moveToNext())
        {
            RutaReparto ruta = new RutaReparto(c.getInt(id),c.getString(codigo),
                    c.getString(nombre), c.getInt(usuario), contarOrdenesReparto(c.getInt(id)));
            Log.d("ruta:", ruta.getCodigo());

            //Se muestran solo las rutas con un numero de ordenes pendientes mayor a cero.
            if (ruta.getNumeroOrdenes() != 0)
                resultado.add(ruta);
        }
        c.close();
        return resultado;
    }


    public boolean existeRutaReparto(RutaReparto ruta)
    {
        boolean resultado = false;
        String[] campos = new String[] {_ID};
        String[] args = new String[] {Integer.toString(ruta.getId())};

        Cursor c = this.getReadableDatabase().query("rutareparto", campos, _ID + "=?", args, null, null, null);
        int id;

        id= c.getColumnIndex(_ID);

        while(c.moveToNext())
        {
            if(c.getInt(id) == ruta.getId())
                resultado = true;
        }

        c.close();
        return resultado;
    }


    private int contarOrdenesReparto(int id){
        SQLiteStatement statement = this.getReadableDatabase().compileStatement("select count(*) from ordenreparto where ruta_reparto_id='" + Integer.toString(id) + "' and estado_reparto_id = '3'");

        return (int) (long) statement.simpleQueryForLong();
    }

    public int totalOrdenesReparto(){
        SQLiteStatement statement = this.getReadableDatabase().compileStatement("select count(*) from ordenreparto where estado_reparto_id = '3'");

        return (int) (long) statement.simpleQueryForLong();
    }
    public int totalOrdenesRepartoVisitado(){
        SQLiteStatement statement = this.getReadableDatabase().compileStatement("select count(*) from ordenreparto where estado_reparto_id = '4'");

        return (int) (long) statement.simpleQueryForLong();
    }

    private int contarOrdenesLectura(int ruta_id, int tipoLectura){
        SQLiteStatement consulta = this.getReadableDatabase().compileStatement("select count(*) from ordenlectura where ruta_id='" + Integer.toString(ruta_id) + "' and estado_lectura_id = '3'");
        return (int) (long) consulta.simpleQueryForLong();
    }



    /**
     * Verifica si es que existe una orden de acuerdo a su id.
     *
     * @param ordenId id de orden de reparto
     * @return boolean que indica si la orden existe dentro de la BD
     */
    public boolean existeOrdenReparto(int ordenId) {
        String filas[] = {_ID};
        String[] args = new String[]{Integer.toString(ordenId)};

        Cursor c = this.getReadableDatabase().query("ordenreparto", filas, _ID + "=?", args, null, null, null);
        if (c.moveToNext())
            return true;

        c.close();
        return false;
    }

    /**
     * Insertar ordenreparto
     *
     * @param reparto
     */
    public boolean insertarOrdenReparto(OrdenReparto reparto)
    {
        if (!this.existeOrdenReparto(reparto.getId())) {
            ContentValues valores = new ContentValues();
            valores.put(_ID, reparto.getId());
            valores.put("codigo", reparto.getCodigo());
            valores.put("direccion", reparto.getDireccion());
            valores.put("secuencia_lector", reparto.getSecuenciaLector());
            valores.put("instalacion_id", reparto.getInstalacionId());
            valores.put("medidor_id", reparto.getMedidorId());
            valores.put("ruta_reparto_id", reparto.getRutaId());
            valores.put("tipo_documento_id", reparto.getTipoDocumentoId());
            valores.put("cliente_id", reparto.getClienteId());
            valores.put("estado_reparto_id", reparto.getEstadoRepartoId()); ;
            valores.put("gps_latitud", reparto.getGpsLatitud());
            valores.put("gps_longitud", reparto.getGpsLongitud());
            valores.put("fecha_ejecucion", reparto.getFechaEjecucion());
            valores.put("numero_cliente", reparto.getNumeroCliente());
            valores.put("flag_envio", 0);
            if(this.getWritableDatabase().insert("ordenreparto", null, valores) != -1)
                return true;
        }
        return false;
    }


    final String filasRepartos[] = {_ID, "codigo", "direccion", "estado_reparto_id","secuencia_lector",  "instalacion_id", "medidor_id", "ruta_reparto_id", "tipo_documento_id","cliente_id", "gps_latitud","gps_longitud","fecha_ejecucion","flag_envio" };

    /**
     * Leer ordenes.
     *
     * @return arreglo con todas las ordenes de la ruta
     */
    public ArrayList<OrdenReparto> listaOrdenesRepartoSinEnviar()
    {
        ArrayList<OrdenReparto> resultado = new ArrayList<>();

        String[] args = new String[] {"4"};

        Cursor c = this.getReadableDatabase().query("ordenreparto", filasRepartos, "estado_reparto_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int direccion = c.getColumnIndex("direccion");
        int estadoRepartoId = c.getColumnIndex("estado_reparto_id");
        int secuenciaLector = c.getColumnIndex("secuencia_lector");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidorId = c.getColumnIndex("medidor_id");
        int rutaId = c.getColumnIndex("ruta_reparto_id");
        int tipoDocumentoId = c.getColumnIndex("tipo_documento_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int flagEnvio = c.getColumnIndex("flag_envio");

        while (c.moveToNext()) {
            if (c.getString(flagEnvio).equals("0")) {
                OrdenReparto orden = new OrdenReparto(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getString(direccion),
                        c.getInt(estadoRepartoId),
                        c.getInt(secuenciaLector),
                        c.getInt (instalacionId),
                        c.getInt(medidorId),
                        c.getInt(rutaId),
                        c.getInt(tipoDocumentoId),
                        c.getInt(clienteId),
                        c.getDouble(gpsLatitud),
                        c.getDouble(gpsLongitud),
                        c.getLong(fechaEjecucion),
                        c.getInt(flagEnvio));

                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }
            }
        }

        Log.d("resultado", resultado.toString());
        c.close();
        return resultado;
    }


    public ArrayList<OrdenReparto> listaOrdenesTerminarReparto()
    {
        ArrayList<OrdenReparto> resultado = new ArrayList<>();

        String[] args = new String[] {"3"};

        Cursor c = this.getReadableDatabase().query("ordenreparto", filasRepartos, "estado_reparto_id=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int direccion = c.getColumnIndex("direccion");
        int estadoRepartoId = c.getColumnIndex("estado_reparto_id");
        int secuenciaLector = c.getColumnIndex("secuencia_lector");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidorId = c.getColumnIndex("medidor_id");
        int rutaId = c.getColumnIndex("ruta_reparto_id");
        int tipoDocumentoId = c.getColumnIndex("tipo_documento_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int flagEnvio = c.getColumnIndex("flag_envio");

        while (c.moveToNext()) {
            if (c.getString(flagEnvio).equals("0")) {
                OrdenReparto orden = new OrdenReparto(
                        c.getInt(id),
                        c.getString (codigo),
                        c.getString(direccion),
                        c.getInt(estadoRepartoId),
                        c.getInt(secuenciaLector),
                        c.getInt (instalacionId),
                        c.getInt(medidorId),
                        c.getInt(rutaId),
                        c.getInt(tipoDocumentoId),
                        c.getInt(clienteId),
                        c.getDouble(gpsLatitud),
                        c.getDouble(gpsLongitud),
                        c.getLong(fechaEjecucion),
                        c.getInt(flagEnvio));

                if (this.buscarCliente(orden.getClienteId()) != null && this.buscarInstalacion(orden.getInstalacionId()) != null
                        && this.buscarMedidor(orden.getMedidorId()) != null) {
                    resultado.add(orden);
                }
            }
        }

        Log.d("resultado", resultado.toString());
        c.close();
        return resultado;
    }




    /**
     * Busca una orden de lectura
     *
     * @return objeto de tipo orden de lectura
     */
    public OrdenReparto buscarOrdenReparto(String numero_cliente) {
        OrdenReparto orden = new OrdenReparto();
        final String filasRepartos[] = {_ID, "codigo", "direccion", "estado_reparto_id","secuencia_lector",  "instalacion_id", "medidor_id", "ruta_reparto_id", "tipo_documento_id","cliente_id", "gps_latitud","gps_longitud","fecha_ejecucion","flag_envio","numero_cliente" };
        String[] args = new String[]{numero_cliente};
        Cursor c = this.getReadableDatabase().query("ordenreparto", filasRepartos, "estado_reparto_id = 3 and numero_cliente=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int codigo = c.getColumnIndex("codigo");
        int direccion = c.getColumnIndex("direccion");
        int estadoRepartoId = c.getColumnIndex("estado_reparto_id");
        int secuenciaLector = c.getColumnIndex("secuencia_lector");
        int instalacionId = c.getColumnIndex("instalacion_id");
        int medidorId = c.getColumnIndex("medidor_id");
        int rutaId = c.getColumnIndex("ruta_reparto_id");
        int tipoDocumentoId = c.getColumnIndex("tipo_documento_id");
        int clienteId = c.getColumnIndex("cliente_id");
        int gpsLatitud = c.getColumnIndex("gps_latitud");
        int gpsLongitud = c.getColumnIndex("gps_longitud");
        int fechaEjecucion = c.getColumnIndex("fecha_ejecucion");
        int flagEnvio = c.getColumnIndex("flag_envio");


        if (c.moveToNext()) {
            orden.setId(c.getInt(id));
            orden.setCodigo(c.getString(codigo));
            orden.setDireccion(c.getString(direccion));
            orden.setDireccion(c.getString(direccion));
            orden.setEstadoRepartoId(estadoRepartoId);
            orden.setSecuenciaLector(secuenciaLector);
            orden.setInstalacionId(c.getInt(instalacionId));
            orden.setMedidorId(c.getInt(medidorId));
            orden.setRutaId(c.getInt(rutaId));
            orden.setTipoDocumentoId(c.getInt(tipoDocumentoId));
            orden.setClienteId(c.getInt(clienteId));
            orden.setGpsLatitud(c.getDouble(gpsLatitud));
            orden.setGpsLongitud(c.getDouble(gpsLongitud));
            orden.setFechaEjecucion(c.getLong(fechaEjecucion));
            orden.setFlagEnvio(c.getInt(flagEnvio));
        }
        c.close();
        return orden;
    }

    /**
     * Actualizar orden.
     *
     * @param orden Objeto de tipo OrdenLectura.
     */
    public void actualizarOrdenReparto(OrdenReparto orden) {
        ContentValues valores = new ContentValues();
        valores.put("estado_reparto_id", orden.getEstadoRepartoId());
        valores.put("gps_latitud", orden.getGpsLatitud());
        valores.put("gps_longitud", orden.getGpsLongitud());;
        valores.put("fecha_ejecucion", orden.getFechaEjecucion());
        valores.put("flag_envio", orden.getFlagEnvio());
        this.getWritableDatabase().update("ordenreparto", valores, _ID + "=" + orden.getId(), null);
    }


    public void terminarReparto(double gps_latitud, double gps_longitud, Long fecha ){
        ArrayList<OrdenReparto> ordenes = this.listaOrdenesTerminarReparto();

        for(final OrdenReparto orden : ordenes){
            ContentValues valores = new ContentValues();
            valores.put("estado_reparto_id", 4);
            valores.put("gps_latitud", gps_latitud);
            valores.put("gps_longitud", gps_longitud);;
            valores.put("fecha_ejecucion", fecha);
            valores.put("flag_envio", 0);
            this.getWritableDatabase().update("ordenreparto", valores, _ID + "=" + orden.getId(), null);

        }
    }

    /**
     * Elimina una orden de lectura dado un id
     *
     * @param id id de orden a eliminar
     */
    public void eliminarOrdenReparto(int id) {
        this.getReadableDatabase().execSQL("DELETE FROM ordenreparto WHERE " + _ID + "=" + Integer.toString(id));
    }

    public TipoDocumento buscaTipoDocumento(int idTipoDocumento)
    {
        TipoDocumento tipoDocumento = new TipoDocumento();
        String filas[] = {_ID};
        String[] args = new String[] {Integer.toString(idTipoDocumento)};

        Cursor c = this.getReadableDatabase().query("tipo_documentos", filas, _ID +"=?", args, null, null, null);

        int id = c.getColumnIndex(_ID);
        int nombre = c.getColumnIndex("nombre");

        if(c.moveToNext())
        {
            tipoDocumento.setId(c.getInt(id));
            tipoDocumento.setNombre(c.getString(nombre));
        }
        c.close();

        return tipoDocumento;
    }


    /**
     * Abre conexion a la base de datos SQLite.
     */
    public void abrir() {
        try {
            this.getWritableDatabase();
        } catch (SQLiteCantOpenDatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cierra conexion a la base de datos SQLite.
     */
    public void cerrar() {
        this.close();
    }


}