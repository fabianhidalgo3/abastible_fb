package cl.jfcor.abastible_fb.Config;

public class Constantes {

    // NOMBRE DE LA BASEDATOS
    public static final String NAME_DATABASE = "abastible_db";

    // VERSION DE DE LA BASEDATOS
    public static final int VERSION_DATABASE = 1;

    // TABLAS TIPO
    public static final String NAME_TABLE_CLIENTES = "clientes";
    public static final String NAME_TABLE_COMUNAS = "comunas";
    public static final String NAME_TABLE_EQUIPOS = "equipos";
    public static final String NAME_TABLE_FOTOGRAFIA = "fotografia";
    public static final String NAME_TABLE_INSTALACIONS = "instalacion";
    public static final String NAME_TABLE_INTENTOS = "intentos";
    public static final String NAME_TABLE_MEDIDOR = "medidor";
    public static final String NAME_TABLE_PERFILS = "perfiles";
    public static final String NAME_TABLE_USUARIOS = "usuarios";

    // TABLAS DE LECTURA
    public static final String NAME_TABLE_ESTADOS_LECTURAS = "estado_lecturas";
    public static final String NAME_TABLE_TIPO_LECTURAS = "tipo_lecturas";
    public static final String NAME_TABLE_CLAVE_LECTURAS = "clave_lecturas";
    public static final String NAME_TABLE_ORDEN_LECTURAS = "orden_lecturas";
    public static final String NAME_TABLE_ASIGNACION_LECTURAS = "asignacion_lecturas";
    public static final String NAME_TABLE_RUTA = "ruta";

    // TABLAS REPARTO
    public static final String NAME_TABLE_ORDEN_REPARTOS = "orden_repartos";
    public static final String NAME_TABLE_ASIGNACION_REPARTOS = "asignacion_repartos";
    public static final String NAME_TABLE_ESTADO_REPARTOS = "estado_repartos";
    public static final String NAME_RUTA_REPARTO = "ruta_repartos";

    // TABLAS CAMBIO DE MEDIDOR
    public static final String NAME_TABLE_ORDEN_CAMBIO_MEDIDOR = "orden_cambio_medidor";
    public static final String NAME_TABLE_CLAVE_CAMBIO_MEDIDOR = "clave_cambio_medidor";
    public static final String NAME_TABLE_ESTADO_CAMBIO_MEDIDOR = "estado_cambio_medidor";
    public static final String NAME_TABLE_FOTOGRAFIA_CAMBIO_MEDIDOR = "fotografia_cambio_medidor";
    public static final String NAME_TABLE_PROCESO_CAMBIO_MEDIDOR = "proceso_cambio_medidor";
    public static final String NAME_TABLE_ASIGNACION_CAMBIO_MEDIDOR = "asignacion_cambio_medidor";
    public static final String NAME_TABLE_RUTA_CAMBIO_MEDIDOR = "ruta_cambio_medidor";

    // TABLAS CORTE REPOSICION
    public static final String NAME_TABLE_ORDEN_CORTE_REPOSICION = "orden_corte_reposicion";
    public static final String NAME_TABLE_CLAVE_CORTE_REPOSICION = "clave_corte_reposicion";
    public static final String NAME_TABLE_ESTADO_CORTE_REPOSICION = "estado_corte_reposicion";
    public static final String NAME_TABLE_FOTOGRAFIA_CORTE_REPOSICION = "fotografia_corte_reposicion";
    public static final String NAME_TABLE_PROCESO_CORTE_REPOSICION = "proceso_corte_reposicion";
    public static final String NAME_TABLE_ASIGNACION_CORTE_REPOSICION = "asignacion_corte_reposicion";
    public static final String NAME_TABLE_RUTA_CORTE_REPOSICION = "ruta_corte_reposicion";
    public static final String NAME_TABLE_TIPO_CORTE_REPOSICION = "tipo_corte_reposicion";

    // TABLAS GAS INICIAL
    public static final String NAME_TABLE_ORDEN_GAS_INICIAL = "orden_gas_inicial";
    public static final String NAME_TABLE_CLAVE_GAS_INICIAL = "clave_gas_inicial";
    public static final String NAME_TABLE_ESTADO_GAS_INICIAL = "estado_gas_inicial";
    public static final String NAME_TABLE_FOTOGRAFIA_GAS_INICIAL = "fotografia_gas_inicial";
    public static final String NAME_TABLE_PROCESO_GAS_INICIAL = "proceso_gas_inicial";
    public static final String NAME_TABLE_ASIGNACION_GAS_INICIAL = "asignacion_gas_inicial";
    public static final String NAME_TABLE_RUTA_GAS_INICIAL = "ruta_gas_inicial";

    // TABLAS CONVENIOS
    public static final String NAME_TABLE_ORDEN_CONVENIO = "orden_convenios";
    public static final String NAME_TABLE_CLAVE_CONVENIO = "clave_convenios";
    public static final String NAME_TABLE_ESTADO_CONVENIO = "estado_convenios";
    public static final String NAME_TABLE_FOTOGRAFIA_CONVENIO = "fotografia_convenios";
    public static final String NAME_TABLE_PROCESO_CONVENIO = "proceso_convenios";
    public static final String NAME_TABLE_ASIGNACION_CONVENIO = "asignacion_convenios";
    public static final String NAME_TABLE_RUTA_CONVENIO = "ruta_convenio";



    public static final String TABLE_PERFILS = "CREATE TABLE perfils (id INTEGER PRIMARY KEY  NOT NULL AUTO_INCREMENT,nombre TEXT DEFAULT NULL,imagen TEXT DEFAULT NULL,created_at TEXT NOT NULL,updated_at TEXT NOT NULL)";

    public static final String TABLE_USUARIOS = "CREATE TABLE usuarios (" +
            "id INTEGER PRIMARY KEY  NOT NULL AUTO_INCREMENT," +
            "email TEXT NOT NULL," +
            "encrypted_password TEXT NOT NULL DEFAULT," +
            "reset_password_token TEXT  NULL DEFAULT," +
            "reset_password_sent_at TEXT NULL DEFAULT," +
            "remember_created_at TEXT NULL DEFAULT," +
            "sign_in_count INTEGER NULL DEFAULT 0," +
            "current_sign_in_at TEXT NULL DEFAULT," +
            "last_sign_in_at TEXT NULL DEFAULT," +
            "current_sign_in_ip TEXT NULL DEFAULTL," +
            "last_sign_in_ip TEXT NULL DEFAULT," +
            "created_at TEXT NOT NULL," +
            "updated_at TEXT NOT NULL," +
            "encrypted_password TEXT NOT NULL," +
            "FOREIGN KEY(perfil_id) REFERENCES perfils(id))";

    public static final String TABLE_ZONAS = "zonas";


    // Datos de Perfiles
    public static final int GOD = 1;
    public static final int Administrador = 2;
    public static final int Coordinador = 3;
    public static final int Analista = 4;
    public static final int Operador = 5;

    // Mensajes Aplicacion
    public static final String MSJ_COMPLETE_DATOS_USERNAME = "Debe ingresar su usuario";
    public static final String MSJ_COMPLETE_DATOS_PASSWORD = "Debe ingresar su contraseña completar los datos requeridos";

    public static final String MSJ_USERNAME_INVALID = "Usuario incorrecto";
    public static final String MSJ_PASSWORD_INVALID = "Contraseña incorrecta";
    public static final String MSJ_USERNAME_ADMIN = "admin";


}
